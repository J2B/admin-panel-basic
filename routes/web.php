<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\ElectricityController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ClientDetailController;
use App\Http\Controllers\MaintenanceController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\BuildingController;
use App\Http\Controllers\AreaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/ajax/cancel_booking', [HomeController::class, 'getCancelBooking']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    // Admin Setting
    Route::resource('roles', RoleController::class)->names([
        'index' => 'admin.roles.index',
        'create' => 'admin.roles.create',
        'edit' => 'admin.roles.edit',
        'destroy' => 'admin.roles.destroy',
        // Add other routes if needed
    ]);
    Route::resource('users', UserController::class)->names([
        'index' => 'admin.users.index',
        'create' => 'admin.users.create',
        // Add other routes if needed
    ]);
    Route::resource('permissions', PermissionController::class)->names([
        'index' => 'admin.permissions.index',
        'create' => 'admin.permissions.create',
        // Add other routes if needed
    ]);
    Route::resource('rooms', RoomController::class)->names([
        'index' => 'admin.rooms.index',
        'create' => 'admin.rooms.create',
    ]);
    Route::resource('electricities', ElectricityController::class);
    Route::resource('clients', ClientController::class);
    Route::post('checkout-client', [ClientController::class,'checkout'])->name('checkout-client');
    Route::resource('client-details', ClientDetailController::class);
    Route::resource('maintenances', MaintenanceController::class);
    Route::resource('expenses', ExpenseController::class);
    Route::resource('areas', AreaController::class);
    Route::resource('buildings', BuildingController::class);
    Route::get('client-details-create/{id}', [ClientDetailController::class,'billing'])->name('client-details-create');
    Route::get('client-details-view/{id}', [ClientDetailController::class,'billingDetail'])->name('client-details-view');
    Route::get('client-details-download/{id}', [ClientDetailController::class,'billingDownload'])->name('client-details-download');
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
    Route::get('/calculate-eb-amount', [ClientDetailController::class, 'calculateEbAmount'])->name('calculate.eb.amount');

    // End Admin Setting
});
