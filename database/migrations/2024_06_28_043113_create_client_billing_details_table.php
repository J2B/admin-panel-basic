<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('client_billing_details', function (Blueprint $table) {
            $table->id();
            $table->string('property_id');
            $table->string('room_id');
            $table->string('client_id');
            $table->string('rent');
            $table->string('eb_unit')->nullable();
            $table->string('eb_id')->nullable();
            $table->string('eb_amount')->nullable();
            $table->string('food')->nullable();
            $table->string('billing_type');
            $table->string('billing_photo_copy')->nullable();
            $table->string('balance_due')->nullable();
            $table->string('month')->nullable();
            $table->date('billing_date')->nullable();
            $table->string('advance')->nullable();
            $table->string('discount')->nullable();
            $table->string('old_due')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('created_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('client_billing_details');
    }
};
