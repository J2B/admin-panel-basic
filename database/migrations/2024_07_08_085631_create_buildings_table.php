<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('area_id')->nullable();
            $table->string('building_number')->nullable();
            $table->string('number_of_room')->nullable();
            $table->string('building_contact_number')->nullable();
            $table->string('building_owner_name')->nullable();
            $table->string('building_owner_contact_number')->nullable();
            $table->string('building_advance_amount')->nullable();
            $table->string('building_type')->nullable();
            $table->string('building_contract_fromdate')->nullable();
            $table->string('building_contract_todate')->nullable();
            $table->string('address')->nullable();
            $table->string('created_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('buildings');
    }
};
