<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('room_id');
            $table->string('property_id');
            $table->string('name');
            $table->string('mobile');
            $table->string('email')->nullable();
            $table->string('photo')->nullable();
            $table->string('client_id')->unique()->nullable();
            $table->string('father_name');
            $table->string('occupation');
            $table->string('residential_address');
            $table->string('id_proof_type')->nullable();
            $table->string('id_proof_number')->nullable();
            $table->string('id_proof_photo')->nullable();
            $table->string('address')->nullable();
            $table->string('deposit')->nullable();
            $table->date('booking_date')->nullable();
            $table->string('status')->default(1)->comment('1.Active 2.Inactive 3.CHECK-IN  4.CHECK-OUT');
            $table->string('created_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};
