<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'maintenance-list',
            'maintenance-create',
            'maintenance-user',
            'maintenance-edit',
            'maintenance-delete',
            'eb-list',
            'eb-create',
            'eb-edit',
            'eb-delete',
            'room-list',
            'room-create',
            'room-edit',
            'room-delete',
            'client-list',
            'client-create',
            'client-edit',
            'client-delete',
            'client_details-list',
            'client_details-create',
            'client_details-edit',
            'client_details-delete',
            'expense-list',
            'expense-create',
            'expense-edit',
            'expense-delete',
            'area-list',
            'area-create',
            'area-edit',
            'area-delete',
            'bulding-list',
            'bulding-create',
            'bulding-edit',
            'bulding-delete'  
        ];

        // Create the 'admin' role
        $role = Role::create(['name' => 'admin']);

        // Create each permission and assign it to the 'admin' role
        foreach ($permissions as $permissionName) {
            $permission = Permission::create(['name' => $permissionName]);
            $role->givePermissionTo($permission);
        }

        // Create an admin user
        $user = User::create([
            'name'              =>  'Admin',
            'property_id'       =>  1,
            'acessproperty_id'  =>  '1,2,3',
            'email'             =>  'admin@gmail.com',
            'password'          =>  Hash::make('Event@123'),
            'mobile'            =>  "8754947759",
            'type'              =>  $role->id,
            'email_verified_at' =>  now(), // Using now() instead of date('Y-m-d')
        ]);

        // Assign the 'admin' role to the created user
        $user->assignRole($role);

        // Define permissions for the 'Users' role
        // $userPermissions = [
        //     'eb-list',
        //     'eb-create',
        //     'eb-edit',
        //     'eb-delete',
        //     'room-list',
        //     'room-create',
        //     'room-edit',
        //     'room-delete'
        // ];

        // Create the 'Users' role
        // $userRole = Role::create(['name' => 'Maintenance']);

        // Assign existing permissions to the 'Users' role
        // foreach ($userPermissions as $permissionName) {
        //     $permission = Permission::where('name', $permissionName)->first();
        //     if ($permission) {
        //         $userRole->givePermissionTo($permission);
        //     }
        //     else{
        //         $permission = Permission::create(['name' => $permissionName]);
        //         $userRole->givePermissionTo($permission);
        //     }
        // }

        // Create a super admin user and assign the 'Users' role
        // $superAdminUser = User::create([
        //     'name'              => 'Super Admin',
        //     'email'             => 'jay@gmail.com',
        //     'password'          =>  Hash::make('jay@123'),
        //     'mobile'            =>  '8754947700',
        //     'type'              =>  $userRole->id,
        //     'email_verified_at' =>  now(),
        // ]);
        // $superAdminUser->assignRole($userRole);

        // $cmsPermissions = [
        //     'eb-list',
        //     'eb-create',
        //     'eb-edit',
        //     'eb-delete',
        //     'room-list',
        //     'room-create',
        //     'room-edit',
        //     'room-delete'
        // ];

        // Create the 'Users' role
        // $cmsRole = Role::create(['name' => 'CMS']);

        // Assign existing permissions to the 'Users' role
        // foreach ($cmsPermissions as $permissionName) {
        //     $permission = Permission::where('name', $permissionName)->first();
        //     if ($permission) {
        //         $cmsRole->givePermissionTo($permission);
        //     }
        //     else{
        //         $permission = Permission::create(['name' => $permissionName]);
        //         $cmsRole->givePermissionTo($permission);
        //     }
        // }

        // Create a cms admin user and assign the 'Users' role
        // $cmsAdminUser = User::create([
        //     'name'              => 'cms Admin',
        //     'email'             => 'jk@gmail.com',
        //     'password'          =>  Hash::make('jk@123'),
        //     'mobile'            =>  '8754947700',
        //     'type'              =>  $userRole->id,
        //     'email_verified_at' =>  now(),
        // ]);
        // $cmsAdminUser->assignRole($userRole);
    }
}

