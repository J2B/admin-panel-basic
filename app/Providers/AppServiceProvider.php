<?php

namespace App\Providers;
use App\Models\Client;
use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        View::composer('layouts.master', function ($view) {
            $user = Auth::user();

            $baseQuery = Client::query();
            

            // Clone the base query to avoid modifying the original query object
            $AllClientQuery      =   clone $baseQuery;
            $AllClient       =   $AllClientQuery->count();
            $view->with('AllClient', $AllClient);
        });
    }
}
