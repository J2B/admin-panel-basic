<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Electricity;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class ElectricityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:eb-list');
         $this->middleware('permission:eb-create', ['only' => ['create','store']]);
         $this->middleware('permission:eb-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:eb-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        // $query  = Electricity::withTrashed()->orderBy('id','DESC');
        $query  = Electricity::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('electricities.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('electricities.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('electricities.name','like', '%'.$name.'%');
            }
        }
        $electricities = $query->paginate(20);
        return view('admin.electricities.index',compact('electricities','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.electricities.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|unique:electricities,name',
            'amount'    => 'required|digits_between:1,10', // adjust 10 to the maximum length you need
        ]);
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $inputs['property_id']  =   Auth::user()->property_id;
        $electricities          =   Electricity::create($inputs);
        return redirect()->route('electricities.index')->with('success','Electricity created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $electricity               =   Electricity::find($id);
        return view('admin.electricities.index',compact('electricity'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $electricity    =   Electricity::find($id);
        $user           =   User::pluck('name','id');
        return view('admin.electricities.edit',compact('electricity','user'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|unique:electricities,name,' . $id,
            'amount'    => 'required|digits_between:1,10', // Adjust based on your requirements
        ]);
        $electricity           =   Electricity::findOrFail($id);
        $electricity->name     =   $request->input('name');
        $electricity->amount   =   $request->input('amount');
        $electricity->status   =   $request->input('status');
        $electricity->save();
        return redirect()->route('electricities.index')->with('success', 'Electricity updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Electricity $electricity)
    {
        $electricity->delete();
        return redirect()->route('electricities.index')->with('success','Electricity deleted successfully');
        
    }
}
