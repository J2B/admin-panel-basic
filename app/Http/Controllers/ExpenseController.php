<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:expense-list');
         $this->middleware('permission:expense-create', ['only' => ['create','store']]);
         $this->middleware('permission:expense-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:expense-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        // $query  = Expense::withTrashed()->orderBy('id','DESC');
        $query  = Expense::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('expenses.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('expenses.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('expenses.name','like', '%'.$name.'%');
            }
        }
        $expenses = $query->paginate(20);
        return view('admin.expenses.index',compact('expenses','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.expenses.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'expense_type'      => 'required',
            'expense_recurrence'=> 'required',
            'date'              => 'required',
            'amount'            => 'required'
        ]);
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $inputs['property_id']  =   Auth::user()->property_id;
        $expenses               =   Expense::create($inputs);
        return redirect()->route('expenses.index')->with('success','Expense created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expense               =   Expense::find($id);
        return view('admin.expenses.index',compact('expense'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense    =   Expense::find($id);
        $user       =   User::pluck('name','id');
        return view('admin.expenses.edit',compact('expense','user'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'expense_type'      => 'required',
            'expense_recurrence'=> 'required',
            'date'              => 'required',
            'amount'            => 'required'
        ]);
        $expense           =   Expense::findOrFail($id);
        $expense->name     =   $request->input('name');
        $expense->amount   =   $request->input('amount');
        $expense->status   =   $request->input('status');
        $expense->save();
        return redirect()->route('expenses.index')->with('success', 'Expense updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
        return redirect()->route('expenses.index')->with('success','Expense deleted successfully');
        
    }
}
