<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\UserController;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use DB;
use Hash;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:user-list');
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->userType = [1=>'Admin',2=>'super Admin',3=>'Person'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   =   Auth::user();
        $query  =   User::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate) && !empty($request->fromdate) && isset($request->todate) && !empty($request->todate))
            {
                $FromDate= date("Y-m-d", strtotime($request->fromdate));
                $ToDate= date("Y-m-d", strtotime($request->todate));
                $query->whereBetween('users.created_at',[$FromDate,$ToDate]);
            }else if($request->fromdate!=""){
                $Sindate= date("Y-m-d", strtotime($request->fromdate));
                $query->where('users.created_at','>=',$Sindate);
            }else if($request->todate!=""){
                $Todate= date("Y-m-d", strtotime($request->todate));
                $query->where('users.created_at','<=',$Todate);
            }   
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('users.name','like', '%'.$name.'%');
            }
            if($request->email)
            {
                $email=$request->email;
                $query->where('users.email',$email);
            }
        }
        $userType   =   $this->userType;
        $count      =   $query->count();
        $data       =   $query->paginate(20);
        return view('admin.users.index',compact('data','userType','request','count'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user       =   Auth::user();
        $roles      =   Role::pluck('name','name')->all();
        $userType   =   $this->userType;
        return view('admin.users.create',compact('roles','userType'));
      
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      =>  'required',
            'password'  =>  'required|same:confirm-password',
            'roles'     =>  'required'
        ]);

        $input                  =   $request->all();
        $input['password']      =   Hash::make($input['password']);
        $user                   =   User::create($input);
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user   =   User::find($id);
        return view('admin::users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user       =   Auth::user();
        $user       =   User::find($id);
        $roles      =   Role::pluck('name','name')->all();
        $userRole   =   $user->roles->pluck('name','name')->all();
        return view('admin.users.edit',compact('user','userRole','roles'));
       
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required'
        ]);
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, ['password']);
        }
        $user = User::findOrFail($id);
        $user->update($input);
        $user->roles()->detach();
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')->with('success','User deleted successfully');
    }    
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        User::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"User Deleted successfully."]);
    }
}
