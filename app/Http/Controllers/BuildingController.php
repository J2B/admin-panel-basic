<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Building;
use App\Models\User;
use App\Models\Area;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class BuildingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:bulding-list');
         $this->middleware('permission:bulding-create', ['only' => ['create','store']]);
         $this->middleware('permission:bulding-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:bulding-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        // $query  = Building::withTrashed()->orderBy('id','DESC');
        $query  = Building::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('buildings.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('buildings.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('buildings.name','like', '%'.$name.'%');
            }
        }
        $buildings = $query->paginate(20);
        return view('admin.buildings.index',compact('buildings','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas  =   Area::where('status',1)->pluck('name','id');
        return view('admin.buildings.create',compact('areas'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->building_type);
        $request->validate([
            'name' => 'required',
            'area_id' => 'required',
            'address' => 'required',
            'building_number' => 'required',
            'number_of_room' => 'required|digits_between:1,5',
            'building_type' => 'required',
            'building_contact_number' => 'required|digits_between:10,15',
            // 'building_owner_name' => 'required_if:building_type=contract',
            // 'building_owner_contact_number' => 'required_if:building_type=contract|digits_between:10,15',
            // 'building_advance_amount' => 'required_if:building_type=contract|digits_between:1,10',
            // 'contract_fromdate' => 'required_if:building_type=contract|date',
            // 'contract_todate' => 'required_if:building_type=contract|date|after_or_equal:contract_fromdate',
        ]);
        
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $buildings               =   Building::create($inputs);
        return redirect()->route('buildings.index')->with('success','Building created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $building               =   Building::find($id);
        return view('admin.buildings.index',compact('building'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $building   =   Building::find($id);
        $areas      =   Area::where('status',1)->pluck('name','id');
        $user       =   User::pluck('name','id');
        return view('admin.buildings.edit',compact('building','user','areas'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|unique:buildings,name,' . $id,
            'pincode'   => 'required', // Adjust based on your requirements
        ]);
        $building           =   Building::findOrFail($id);
        $building->name     =   $request->input('name');
        $building->amount   =   $request->input('amount');
        $building->status   =   $request->input('status');
        $building->save();
        return redirect()->route('buildings.index')->with('success', 'Building updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Building $building)
    {
        $building->delete();
        return redirect()->route('buildings.index')->with('success','Building deleted successfully');
        
    }
}
