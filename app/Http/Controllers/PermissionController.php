<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:permission-list');
         $this->middleware('permission:permission-create', ['only' => ['create','store']]);
         $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user               =   Auth::user();
        $permissions        =   $user->permissions; // collection of permission objects
        $permissionNames    =   $user->getPermissionNames(); // collection of name strings
        $query              =   Permission::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate) && !empty($request->fromdate) && isset($request->todate) && !empty($request->todate))
            {
                $FromDate= date("Y-m-d", strtotime($request->fromdate));
                $ToDate= date("Y-m-d", strtotime($request->todate));
                $query->whereBetween('permissions.created_at',[$FromDate,$ToDate]);
            }else if($request->fromdate!=""){
                $Sindate= date("Y-m-d", strtotime($request->fromdate));
                $query->where('permissions.created_at','>=',$Sindate);
            }else if($request->todate!=""){
                $Todate= date("Y-m-d", strtotime($request->todate));
                $query->where('permissions.created_at','<=',$Todate);
            }   
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('permissions.name','LIKE','%'.$name.'%');
            }
        }
        $count          =   $query->count();
        $permissions    =   $query->paginate(20);
        return view('admin.permissions.index',compact('permissions','count','request'))->with('i', (request()->input('page', 1) - 1) * 20);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.permissions.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:permissions,name|max:255'
        ]);
        Permission::create([
            'name' => $request->input('name')
        ]);
        return redirect()->back()->with('success', 'Permission created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        return view('admin.permissions.show',compact('permission'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit',compact('permission'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $request->validate([
            'name' => 'required|string|unique:permissions,name,' . $permission->id . '|max:255'
        ]);
        $permission->update([
            'name' => $request->input('name')
        ]);
        return redirect()->route('admin.permissions.index')->with('success', 'Permission updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();
        return redirect()->route('admin.permissions.index')->with('success','Permission deleted successfully');
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        Permission::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Permission Deleted successfully."]);
    }
}
