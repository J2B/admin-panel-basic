<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:role-list');
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $query = Role::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('roles.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('roles.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('roles.name','like', '%'.$name.'%');
            }
        }
        $roles = $query->paginate(20);
        return view('admin.roles.index',compact('roles','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission     =   Permission::get();
        return view('admin.roles.create',compact('permission'));
    
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|unique:roles,name',
            'permission'    => 'required',
        ]);
        $role   =   Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('roles.index')->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role               =   Role::find($id);
        $rolePermissions    =   Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")->where("role_has_permissions.role_id",$id)->get();
        return view('admin.roles.show',compact('role','rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role               =   Role::find($id);
        $permission         =   Permission::get();
        $rolePermissions    =   DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
        return view('admin.roles.edit',compact('role','permission','rolePermissions'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the request
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required|array', // Ensure permissions are provided as an array
            // 'permission.*' => 'exists:permissions,name' // Ensure each permission exists
        ]);

        // Find the role by ID
        $role = Role::find($id);

        if (!$role) {
            return redirect()->route('roles.index')->with('error', 'Role not found.');
        }

        // Update the role name
        $role->name = $request->input('name');
        $role->save();

        
        // Get permission IDs from the request
        $permissionIds = $request->input('permission');

        // Fetch permission names using the IDs
        $permissions = Permission::whereIn('id', $permissionIds)->get();

        // Debugging output to check permissions
        \Log::info('Permissions:', $permissions->pluck('name')->toArray());

        // Sync permissions
        $role->syncPermissions($permissions);
        // Sync permissions
        // $role->syncPermissions($request->input('permission')); // Get the permission input
        // Redirect with success message
        return redirect()->route('admin.roles.index')->with('success', 'Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')->with('success','Role deleted successfully');
        
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        Role::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Role Deleted successfully."]);
    }
}
