<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\ClientBillingDetail;
use App\Models\CheckoutDetail;
use App\Models\Maintenance;
use Carbon\Carbon;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
            $currentMonth = Carbon::now()->format('F-Y');
            // dd($currentMonth);
            // $currentMonth       =   Carbon::now()->format('F');
            $user               =   Auth::user();
            $AllClient          =   Client::count();
            $AllCheckinClient   =   Client::where('status',3)->count();
            $AllCheckoutClient  =   Client::where('status',4)->count();
            $Alldeposit         =   Client::sum('deposit');
            $checkin            =   Client::whereYear('booking_date', Carbon::now()->year)->whereMonth('booking_date', Carbon::now()->month)->where('status',3)->count();
            $deposit            =   Client::whereYear('booking_date', Carbon::now()->year)->whereMonth('booking_date', Carbon::now()->month)->where('status',3)->sum('deposit');
            $sums               =   ClientBillingDetail::where('month', $currentMonth)->selectRaw('SUM(rent) as total_rent, SUM(food) as total_food, SUM(eb_amount) as total_eb,SUM(paid_amount) as total_paid')->first();
            // Extract the values from the result
            $rent               =   $sums->total_rent;
            $food               =   $sums->total_food;
            $paidAmount         =   $sums->total_paid;
            $eb                 =   $sums->total_eb;
            $checkout           =   CheckoutDetail::whereYear('checkout_date', Carbon::now()->year)->whereMonth('checkout_date', Carbon::now()->month)->where('status',1)->count();
            $maintenance        =   Maintenance::whereYear('date', Carbon::now()->year)->whereMonth('date', Carbon::now()->month)->SUM('paid_amount');
        return view('dashboard',compact('AllClient','AllCheckinClient','AllCheckoutClient','checkin','deposit','rent','food','eb','paidAmount','Alldeposit','checkout','maintenance'));
    }
}
