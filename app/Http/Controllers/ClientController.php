<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage; // Import Storage facade
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\User;
use App\Models\Room;
use App\Models\CheckoutDetail;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:client-list');
         $this->middleware('permission:client-create', ['only' => ['create','store']]);
         $this->middleware('permission:client-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:client-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        // $query  = Client::withTrashed()->orderBy('id','DESC');
        $query  = Client::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('clients.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('clients.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('clients.name','like', '%'.$name.'%');
            }
        }
        $clients = $query->paginate(20);
        return view('admin.clients.index',compact('clients','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Room::all()->filter(function($room) {
            return ClientController::getRoomCount($room->id) == 1;
        });
        // $rooms  =   Room::where('status',1)->pluck('name','id');
        return view('admin.clients.create',compact('rooms'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'                      =>  'required|string|max:255',
            'email'                     =>  'required|email|max:255',
            'mobile'                    =>  'required|digits:10',
            'occupation'                =>  'required|string|max:255',
            'id_proof_number'           =>  'required|string|max:255',
            'room_id'                   =>  'required|exists:rooms,id',
            'residential_address'       =>  'required|string|max:1000',
            'father_name'               =>  'required|string|max:255',
            'id_proof_type'             =>  'required|string|in:Student ID,Passport,Driving License,Aadhar Card,Ration card,Voter ID',
            'deposit'                   =>  'required|numeric|min:0',
            'address'                   =>  'required|string|max:1000',
            'photo'                     =>  'required|image|mimes:jpeg,png,jpg|max:1024',
            'id_proof_photo'            =>  'required|image|mimes:jpeg,png,jpg|max:1024',
        ]);     
        
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $inputs['status']       =   4;
        //  Handle photo upload
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = uniqid() . '.' . $photo->getClientOriginalExtension();
            $path = Storage::putFileAs('public/photos', $photo, $photoName);
            $inputs['photo'] = basename($path);
        }

        // Handle ID proof photo upload
        if ($request->hasFile('id_proof_photo')) {
            $idProofPhoto = $request->file('id_proof_photo');
            $idProofPhotoName = uniqid() . '.' . $idProofPhoto->getClientOriginalExtension();
            $path = Storage::putFileAs('public/photo_copy', $idProofPhoto, $idProofPhotoName);
            $inputs['id_proof_photo'] = basename($path);
        }
        $clients                =   Client::create($inputs);
        $client_id              =   $request->room_id . date('Y') . $clients->id;
        $clients->update(['client_id' => $client_id]);
        $inputs['property_id']  =   Auth::user()->property_id;
        $clients->save();
        return redirect()->route('clients.index')->with('success','Client created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client     =   Client::find($id);
        $user       =   User::pluck('name','id');
        $rooms      =   Room::where('status',1)->pluck('name','id');
        return view('admin.clients.show',compact('client','rooms','user'));
    }

    public function checkout(Request $request)
    {
        $request->validate([
            'return_deposit'    =>  'required',
            'reason'            =>  'required',
            'checkout_date'     =>  'required',
        ]);     
        
        $inputs['return_deposit']   =   $request->return_deposit;
        $inputs['reason']           =   $request->reason;
        $inputs['checkout_date']    =   $request->checkout_date;
        $inputs['client_id']        =   $request->client_id;
        $inputs['created_by']       =   Auth::user()->id;
        CheckoutDetail::create($inputs);
        $update                     =   Client::find($request->client_id)->update(['status'=>4]);
        return redirect()->back()->with('success','Checkout SucessFully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client     =   Client::find($id);
        $user       =   User::pluck('name','id');
        $rooms = Room::all()->filter(function($room) {
            return ClientController::getRoomCount($room->id) == 1;
        });
        // $rooms      =   Room::where('status',1)->pluck('name','id');
        return view('admin.clients.edit',compact('client','user','rooms'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
{
    // Validate the request
    $request->validate([
        'name'                  =>  'required|string|max:255',
        'email'                 =>  'required|email|max:255',
        'mobile'                =>  'required|digits:10',
        'occupation'            =>  'required|string|max:255',
        'id_proof_number'       =>  'required|string|max:255',
        'room_id'               =>  'required|exists:rooms,id',
        'residential_address'   =>  'required|string|max:255',
        'father_name'           =>  'required|string|max:255',
        'id_proof_type'         =>  'required|string|max:255',
        'deposit'               =>  'required|numeric',
        'address'               =>  'required|string|max:255',
        'photo'                 =>  'nullable|image|mimes:jpg,jpeg,png|max:1024', // 1MB Max
        'id_proof_photo'        =>  'nullable|image|mimes:jpg,jpeg,png|max:1024', // 1MB Max
    ]);

    // Find the client by ID
    $client = Client::findOrFail($id);

    // Update client data
    $client->name = $request->input('name');
    $client->email = $request->input('email');
    $client->mobile = $request->input('mobile');
    $client->occupation = $request->input('occupation');
    $client->id_proof_number = $request->input('id_proof_number');
    $client->room_id = $request->input('room_id');
    $client->residential_address = $request->input('residential_address');
    $client->father_name = $request->input('father_name');
    $client->id_proof_type = $request->input('id_proof_type');
    $client->deposit = $request->input('deposit');
    $client->address = $request->input('address');

    // Handle file uploads
    if ($request->hasFile('photo')) {
        // Delete the old photo if exists
        if ($client->photo) {
            Storage::delete('public/photos' . $client->photo);
        }
        // Delete the old photo if it exists
        if ($client->photo) {
            $photoPath = 'public/photos/' . $client->photo;

            // Check if the file exists before attempting deletion
            if (Storage::exists($photoPath)) {
                Storage::delete($photoPath);
            }
        }

        // Store the new photo
        $photoPath = $request->file('photo')->store('photos', 'public');
        $client->photo = $photoPath;
    }

    if ($request->hasFile('id_proof_photo')) {
        // Delete the old ID proof photo if exists
        if ($client->id_proof_photo) {
            Storage::delete('public/photo_copy' . $client->id_proof_photo);
        }
        // Store the new ID proof photo
        $idProofPhotoPath = $request->file('id_proof_photo')->store('photo_copy', 'public');
        $client->id_proof_photo = $idProofPhotoPath;
    }

    // Save the client
    $client->save();

    // Redirect back with success message
    return redirect()->route('clients.index')->with('success', 'Client updated successfully!');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('clients.index')->with('success','Client deleted successfully');
        
    }
    static public function getRoomCount($id)
    {
        $clientCount = Client::whereIn('status', [1, 3])->where('room_id', $id)->count();
        $room = Room::find($id);

        if (!$room) {
            return 0; // Return 0 if the room doesn't exist
        }

        $roomCapacity = $room->max_no;
        $availableSpace = $roomCapacity - $clientCount;

        return $availableSpace > 0 ? 1 : 0;
    }
}
