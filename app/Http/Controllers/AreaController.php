<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:area-list');
         $this->middleware('permission:area-create', ['only' => ['create','store']]);
         $this->middleware('permission:area-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:area-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        // $query  = Area::withTrashed()->orderBy('id','DESC');
        $query  = Area::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('areas.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('areas.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('areas.name','like', '%'.$name.'%');
            }
        }
        $areas = $query->paginate(20);
        return view('admin.areas.index',compact('areas','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.areas.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|unique:areas,name',
            'pincode'   => 'required', // adjust 10 to the maximum length you need
        ]);
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $areas                  =   Area::create($inputs);
        return redirect()->route('areas.index')->with('success','Area created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $area               =   Area::find($id);
        return view('admin.areas.index',compact('area'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area    =   Area::find($id);
        $user    =   User::pluck('name','id');
        return view('admin.areas.edit',compact('area','user'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|unique:areas,name,' . $id,
            'pincode'   => 'required', // Adjust based on your requirements
        ]);
        $area           =   Area::findOrFail($id);
        $area->name     =   $request->input('name');
        $area->pincode  =   $request->input('pincode');
        $area->status   =   $request->input('status');
        $area->save();
        return redirect()->route('areas.index')->with('success', 'Area updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        $area->delete();
        return redirect()->route('areas.index')->with('success','Area deleted successfully');
        
    }
}
