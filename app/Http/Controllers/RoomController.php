<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Room;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:room-list');
         $this->middleware('permission:room-create', ['only' => ['create','store']]);
         $this->middleware('permission:room-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:room-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        // $query  = Room::withTrashed()->orderBy('id','DESC');
        $query  = Room::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('rooms.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('rooms.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('rooms.name','like', '%'.$name.'%');
            }
        }
        $rooms = $query->paginate(20);
        return view('admin.rooms.index',compact('rooms','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rooms.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|unique:rooms,name',
            'door_no'   => 'required|unique:rooms,door_no',
            'max_no'    => 'required|digits_between:1,10', // adjust 10 to the maximum length you need
            'type'      => 'required',
        ]);
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $inputs['property_id']  =   Auth::user()->property_id;
        $rooms                  =   Room::create($inputs);
        return redirect()->route('admin.rooms.index')->with('success','Room created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $room               =   Room::find($id);
        return view('admin.rooms.index',compact('room'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room  =   Room::find($id);
        return view('admin.rooms.edit',compact('room'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|unique:rooms,name,' . $id,
            'door_no'   => 'required|unique:rooms,door_no,' . $id,
            'max_no'    => 'required|digits_between:1,10', // Adjust based on your requirements
            'type'      => 'required',
            'status'    => 'required'
        ]);
        $room           =   Room::findOrFail($id);
        $room->name     =   $request->input('name');
        $room->door_no  =   $request->input('door_no');
        $room->max_no   =   $request->input('max_no');
        $room->type     =   $request->input('type');
        $room->status   =   $request->input('status');
        $room->save();
        return redirect()->route('admin.rooms.index')->with('success', 'Room updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return redirect()->route('admin.rooms.index')->with('success','Room deleted successfully');
        
    }
}
