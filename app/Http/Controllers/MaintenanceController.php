<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Maintenance;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;
use PDF;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:maintenance-list');
         $this->middleware('permission:maintenance-create', ['only' => ['create','store']]);
         $this->middleware('permission:maintenance-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:maintenance-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        $query  = Maintenance::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('maintenances.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('maintenances.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('maintenances.name','like', '%'.$name.'%');
            }
        }
        $maintenances = $query->paginate(20);
        return view('admin.maintenances.index',compact('maintenances','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.maintenances.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'date' => 'required'
        ], [
            'name.required' => 'Please enter the Name.',
            'date.required' => 'Please enter the Date.',
        ]);
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $inputs['property_id']  =   Auth::user()->property_id;
        //  Handle photo upload
        if ($request->hasFile('photo_copy')) {
            $photo = $request->file('photo_copy');
            $photoName = uniqid() . '.' . $photo->getClientOriginalExtension();
            $path = Storage::putFileAs('public/photo_copy', $photo, $photoName);
            $inputs['photo_copy'] = basename($path);
        }
        $maintenances           =   Maintenance::create($inputs);
        return redirect()->route('maintenances.index')->with('success','Maintenance created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $maintenance    =   Maintenance::pluck('id','name');
        return view('admin.maintenances.show',compact('client','maintenance','client_details'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maintenance    =   Maintenance::find($id);
        return view('admin.maintenances.edit',compact('maintenance'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required',
            'date'    => 'required'
        ]);
        $maintenance            =   Maintenance::findOrFail($id);
        $maintenance->name      =   $request->input('name');
        $maintenance->date      =   $request->input('date');
        // Handle file uploads
    if ($request->hasFile('photo_copy')) {
        // Delete the old photo if exists
        if ($maintenance->photo_copy) {
            Storage::delete('public/photo_copy' . $maintenance->photo_copy);
        }
        // Delete the old photo if it exists
        if ($maintenance->photo_copy) {
            $photoPath = 'public/photos/' . $maintenance->photo_copy;

            // Check if the file exists before attempting deletion
            if (Storage::exists($photoPath)) {
                Storage::delete($photoPath);
            }
        }

        // Store the new photo
        $photoPath = $request->file('photo_copy')->store('photo_copy', 'public');
        $maintenance->photo_copy = $photoPath;
    }
        $maintenance->save();
        return redirect()->route('maintenances.index')->with('success', 'Maintenance updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Maintenance $maintenance)
    {
        $maintenance->delete();
        return redirect()->route('admin.maintenances.index')->with('success','Maintenance deleted successfully');
        
    }
}
