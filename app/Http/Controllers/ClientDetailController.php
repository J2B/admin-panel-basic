<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\Client;
use App\Models\Electricity;
use App\Models\ClientBillingDetail;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;
use PDF;

class ClientDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:client_details-list');
         $this->middleware('permission:client_details-create', ['only' => ['create','store']]);
         $this->middleware('permission:client_details-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:client_details-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user   = Auth::user();
        $query  = Room::orderBy('id','DESC');
        if(isset($request->search))
        {
            if(isset($request->fromdate))
            {
                if($request->fromdate!=""){
                    $Sindate= date("Y-m-d", strtotime($request->fromdate));
                    $query->where('rooms.created_at','>=',$Sindate);
                } 
            }
            if(isset($request->todate))
            {
                if($request->todate!=""){
                    $Todate= date("Y-m-d", strtotime($request->todate. ' +1 day'));
                    $query->where('rooms.created_at','<=',$Todate);
                }  
            }
           
            if($request->name)
            {
                $name=$request->name;
                $query->where('rooms.name','like', '%'.$name.'%');
            }
        }
        $rooms = $query->paginate(20);
        return view('admin.client-details.index',compact('rooms','request'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
       
    }

    static public function getCount($id)
    {
        $client     =   Client::whereIn('status',[1,3])->count();
        return $client;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        dd($request);
        return view('admin.client-details.create');
    }

    public function billing($id)
    {
        $client         =   Client::find($id);
        $electricity    =   Electricity::pluck('name','id');
        $room           =   Room::pluck('name','id');
        $client_details =   ClientBillingDetail::where('client_id',$id)->orderBy('id','DESC')->first();
        return view('admin.client-details.create',compact('client','room','electricity','client_details'));
    }
    public function billingDetail($id)
    {
        set_time_limit(120);
        $client_detail  =   ClientBillingDetail::find($id);
        $client         =   Client::where('id',$client_detail->client_id)->first();
        $electricity    =   Electricity::pluck('name','id');
        $room           =   Room::pluck('name','id');
        return view('admin.client-details.billingdetail',compact('client','room','electricity','client_detail'));
    }
    public function billingDownload($id)
    {
        $client_detail  =   ClientBillingDetail::find($id);
        $client         =   Client::where('id',$client_detail->client_id)->first();
        $room           =   Room::where('id',$client->room_id)->first();
        $pdf            =   PDF::loadview('admin.client-details.download',compact('client','room','client_detail'));
        // return view('admin.client-details.download',compact('client','room','client_detail'));
        // Format the filename
        $client_name = str_replace(' ', '_', $client->name); // Replace spaces with underscores
        $billing_month = str_replace(' ', '_', $client_detail->month); // Replace spaces with underscores
        $filename = "{$client_name}_{$billing_month}_Billing_Detail.pdf";

        return $pdf->download($filename);
    }
    public function calculateEbAmount(Request $request)
    {
        $ebUnit         =   $request->input('eb_unit');
        $ebid           =   $request->input('eb_id');
        if(isset($ebid) && $ebid!='')
        {
            $electricty     =   Electricity::find($ebid);
            $ebAmount       =   $ebUnit * $electricty->amount; // Example calculation
        }
        else{
            $ebAmount       =   1; // Example calculation
        }
        return response()->json(['eb_amount' => $ebAmount]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'advance' => 'required',
            'rent' => 'required|numeric',
            'eb_id' => 'required',
            'eb_unit' => 'required|numeric',
            'room_id' => 'required',
            'month' => 'required',
            'year' => 'required',
            'billing_type' => 'required',
            'paid_amount' => 'required|numeric',
            'billing_photo_copy' => 'required_if:billing_type,Online Payment|image|max:1024', // Max size in KB
        ], [
            'advance.required' => 'Please enter the Advance.',
            'rent.required' => 'Please enter the client\'s Rent.',
            'rent.numeric' => 'Rent must be a numeric value.',
            'eb_id.required' => 'Please enter the client\'s EB Name.',
            'eb_unit.required' => 'Please enter the client\'s EB Unit.',
            'eb_unit.numeric' => 'EB Unit must be a numeric value.',
            'room_id.required' => 'Please select a room.',
            'month.required' => 'Please select a month.',
            'year.required' => 'Please select a year.',
            'billing_type.required' => 'Please select Billing Type.',
            'paid_amount.required' => 'Please enter the Paid amount.',
            'paid_amount.numeric' => 'Paid amount must be a numeric value.',
            'billing_photo_copy.required_if' => 'Please upload a client photo for Online Payment.',
            'billing_photo_copy.image' => 'Billing photo must be an image (JPEG, PNG, JPG).',
            'billing_photo_copy.max' => 'File size must be less than :max KB.',
        ]);
        $inputs                 =   $request->all();
        $inputs['created_by']   =   Auth::user()->id;
        $inputs['month']        =   $request->month. '-' .$request->year;
        $inputs['balace_due']   =   $request->total_amount - $request->paid_amount;
        $inputs['property_id']  =   Auth::user()->property_id;
        $rooms                  =   ClientBillingDetail::create($inputs);
        return redirect()->route('admin.rooms.index')->with('success','Room created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client         =   Client::find($id);
        $client_details =   ClientBillingDetail::where('client_id',$id)->get();
        $room           =   Room::pluck('id','name');
        return view('admin.client-details.show',compact('client','room','client_details'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room       =   Room::find($id);
        $clients    =   Client::whereIn('status',[1,3])->where('room_id',$id)->get();
        return view('admin.client-details.edit',compact('clients','room'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|unique:rooms,name,' . $id,
            'door_no'   => 'required|unique:rooms,door_no,' . $id,
            'max_no'    => 'required|digits_between:1,10', // Adjust based on your requirements
            'type'      => 'required',
            'status'    => 'required'
        ]);
        $room           =   Room::findOrFail($id);
        $room->name     =   $request->input('name');
        $room->door_no  =   $request->input('door_no');
        $room->max_no   =   $request->input('max_no');
        $room->type     =   $request->input('type');
        $room->status   =   $request->input('status');
        $room->save();
        return redirect()->route('admin.rooms.index')->with('success', 'Room updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return redirect()->route('admin.rooms.index')->with('success','Room deleted successfully');
        
    }
}
