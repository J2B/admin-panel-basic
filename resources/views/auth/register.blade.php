@extends('layouts.app')  @php $page_title = 'PG HOSTING-Registration'; $page_name = 'PG HOSTING-Registration'; @endphp

@section('content')
<div class="container">

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
            <div class="col-lg-7">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                    </div>
                    <form class="user" action="{{ route('register') }}" method="POST" id="userCreation">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name" id="name">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-user" id="mobile" name="mobile"
                                    placeholder="Mobile">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control form-control-user" name="email" id="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control form-control-user"
                                name="password" id="password" required autocomplete="new-password" placeholder="New Password">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control form-control-user"
                                name="password_confirmation" id="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Register Account
                        </button>
                        <!-- <a href="login.html" class="btn btn-primary btn-user btn-block">
                            Register Account
                        </a>
                        <hr>
                        <a href="index.html" class="btn btn-google btn-user btn-block">
                            <i class="fab fa-google fa-fw"></i> Register with Google
                        </a>
                        <a href="index.html" class="btn btn-facebook btn-user btn-block">
                            <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                        </a> -->
                    </form>
                    <hr>
                    <!-- <div class="text-center">
                        <a class="small" href="forgot-password.html">Forgot Password?</a>
                    </div> -->
                    <div class="text-center">
                        <a class="small" href="{{route('login')}}">Already have an account? Login!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        $("#userCreation").validate({
			rules: {
                name: "required",
                email: {
                    required: true,
                    email: true  // Ensure the input is a valid email address
                },
                password: {
                    required: true,
                    minlength: 8,
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password"
                },
                mobile: {
                    required: true,
                    digits: true,  // Allow only digits
                    minlength: 10, // Require at least 10 digits
                    maxlength: 10  // Allow a maximum of 10 digits
                }
            },
			messages: {
                name: "Please Enter Name",
                email: {
                    required: "Please enter your email",
                    email: "Please enter a valid email address"
                },
                password: {
                    required: "Please enter your Password",
                    minlength: "Password must be at least 8 characters",
                },
                password_confirmation: {
                    required: "Please confirm your password",
                    equalTo: "Passwords do not match"
                },
                mobile: {
                    required: "Please enter your mobile number",
                    minlength: "Mobile number must be at least 10 characters",
                    maxlength: "Mobile number must not exceed 10 characters",
                    digits: "Please enter only digits"
                }
			}
		});
    });
</script>
@endsection
