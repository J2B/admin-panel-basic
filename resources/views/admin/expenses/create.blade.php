@extends('layouts.master')

@php
    $page_title = 'Add Expense';
    $page_name = 'Add Expense';
@endphp

@section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- DataTales Example -->
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Add Expense</h6>
            <a href="{{ route('expenses.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('expenses.store') }}" id="expenseCreation">
                @csrf
                <input type="hidden" name="property_id" value="{{ Auth::user()->property_id ?? 1 }}">
                <div class="form-row">
                    <!-- Expense Type Dropdown -->
                    <div class="form-group col-md-4">
                        <label for="expense_type">Expense Type</label>
                        <select name="expense_type" id="expense_type" class="form-control" onchange="toggleOtherExpense(this)">
                            <option value="" {{ old('expense_type') == '' ? 'selected' : '' }}>Expense Type</option>
                            <option value="Food Expense" {{ old('expense_type') == 'Food Expense' ? 'selected' : '' }}>Food Expense</option>
                            <option value="Electricity Bill" {{ old('expense_type') == 'Electricity Bill' ? 'selected' : '' }}>Electricity Bill</option>
                            <option value="Cable Bill" {{ old('expense_type') == 'Cable Bill' ? 'selected' : '' }}>Cable Bill</option>
                            <option value="Internet Bill" {{ old('expense_type') == 'Internet Bill' ? 'selected' : '' }}>Internet Bill</option>
                            <option value="Water Bill" {{ old('expense_type') == 'Water Bill' ? 'selected' : '' }}>Water Bill</option>
                            <option value="Maintenance" {{ old('expense_type') == 'Maintenance' ? 'selected' : '' }}>Maintenance</option>
                            <option value="Employee Salaries" {{ old('expense_type') == 'Employee Salaries' ? 'selected' : '' }}>Employee Salaries</option>
                            <option value="Property Rent" {{ old('expense_type') == 'Property Rent' ? 'selected' : '' }}>Property Rent</option>
                            <option value="Property Tax" {{ old('expense_type') == 'Property Tax' ? 'selected' : '' }}>Property Tax</option>
                            <option value="Others" {{ old('expense_type') == 'Others' ? 'selected' : '' }}>Others</option>
                            <option value="Groceries" {{ old('expense_type') == 'Groceries' ? 'selected' : '' }}>Groceries</option>
                            <option value="Gas" {{ old('expense_type') == 'Gas' ? 'selected' : '' }}>Gas</option>
                            <option value="Housekeeping" {{ old('expense_type') == 'Housekeeping' ? 'selected' : '' }}>Housekeeping</option>
                            <option value="Promotions" {{ old('expense_type') == 'Promotions' ? 'selected' : '' }}>Promotions</option>
                            <option value="Miscellaneous" {{ old('expense_type') == 'Miscellaneous' ? 'selected' : '' }}>Miscellaneous</option>
                            <option value="Purchases" {{ old('expense_type') == 'Purchases' ? 'selected' : '' }}>Purchases</option>
                            <option value="Transport" {{ old('expense_type') == 'Transport' ? 'selected' : '' }}>Transport</option>
                            <option value="Telephone Bill" {{ old('expense_type') == 'Telephone Bill' ? 'selected' : '' }}>Telephone Bill</option>
                            <option value="Water Tanker" {{ old('expense_type') == 'Water Tanker' ? 'selected' : '' }}>Water Tanker</option>
                            <option value="Staff Advance" {{ old('expense_type') == 'Staff Advance' ? 'selected' : '' }}>Staff Advance</option>
                        </select>
                    </div>

                    <!-- Other Expense Type Input -->
                    <div class="form-group col-md-4" id="other_expense_group" style="display: none;">
                        <label for="other_expense_type">Other Expense Type</label>
                        <input type="text" class="form-control" id="other_expense_type" name="other_expense_type" value="{{ old('other_expense_type') }}" placeholder="Other Expense Type"/>
                    </div>

                    <!-- Expense Recurrence Dropdown -->
                    <div class="form-group col-md-4">
                        <label for="expense_recurrence">Expense Recurrence</label>
                        <select name="expense_recurrence" id="expense_recurrence" class="form-control">
                            <option value="" {{ old('expense_recurrence') == '' ? 'selected' : '' }}>Expense Recurrence</option>
                            <option value="Daily" {{ old('expense_recurrence') == 'Daily' ? 'selected' : '' }}>Daily</option>
                            <option value="Weekly" {{ old('expense_recurrence') == 'Weekly' ? 'selected' : '' }}>Weekly</option>
                            <option value="Monthly" {{ old('expense_recurrence') == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                            <option value="Quarterly" {{ old('expense_recurrence') == 'Quarterly' ? 'selected' : '' }}>Quarterly</option>
                            <option value="Yearly" {{ old('expense_recurrence') == 'Yearly' ? 'selected' : '' }}>Yearly</option>
                            <option value="One-Time" {{ old('expense_recurrence') == 'One-Time' ? 'selected' : '' }}>One-Time</option>
                        </select>
                    </div>

                    <!-- Amount Input -->
                    <div class="form-group col-md-4">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount') }}" placeholder="Amount"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="amount">Date</label>
                        <input type="text" class="form-control air-datepicker" autocomplete="off" data-date-format="yyyy-mm-dd" id="date" name="date" value="{{ old('date') }}" placeholder="Date" readonly/>
                    </div>
                </div>

                <!-- Comment Textarea -->
                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea class="form-control" name="comment" id="comment">{{ old('comment') }}</textarea>
                </div>
               
                <!-- Submit Button -->
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    if ($.fn.datepicker !== undefined) {
        $('.air-datepicker').datepicker({
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'Y-m-d',
                firstDay: 0
            }
        });
    }
</script>
<script>
    function toggleOtherExpense(select) {
        var otherExpenseGroup = document.getElementById('other_expense_group');
        if (select.value === 'Others') {
            otherExpenseGroup.style.display = 'block';
        } else {
            otherExpenseGroup.style.display = 'none';
        }
    }

    document.addEventListener('DOMContentLoaded', function() {
        var expenseType = document.getElementById('expense_type');
        if (expenseType.value === 'Others') {
            document.getElementById('other_expense_group').style.display = 'block';
        }
    });
</script>

<script>
    $(document).ready(function () {
        $.validator.addMethod("otherExpenseRequired", function (value, element) {
            return $("#expense_type").val() !== "Others" || value.trim() !== "";
        }, "Please enter the other expense type");
        $("#expenseCreation").validate({
            rules: {
                expense_type: "required",
                amount: {
                    required: true,
                    digits: true,
                },
                expense_recurrence: "required",
                date: "required",
                comment: "required",
                other_expense_type: {
                    otherExpenseRequired: true
                }
            },
            messages: {
                expense_type: "Please select an expense type",
                amount: {
                    required: "This field is required",
                    digits: "Please enter only digits",
                },
                expense_recurrence: "Please select an expense recurrence",
                date: "Please Chooose Date",
                comment: "Please Enter Some text",
                other_expense_type: "Please enter the other expense type"
            }
        });
           // Show or hide the other expense type field based on the selected expense type
        $('#expense_type').change(function () {
            toggleOtherExpense(this);
            $("#other_expense_type").valid();
        }) // Trigger validation on change
    });
</script>
@endsection
