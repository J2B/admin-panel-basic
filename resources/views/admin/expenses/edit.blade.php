@extends('layouts.master') @php $page_title = 'View Expense'; $page_name = 'View Expense'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">View Expense</h6>
            <a href="{{ route('expenses.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('expenses.update', $expense->id) }}" id="expenseCreation">
                @csrf @method('PUT')
                <input type="hidden" value="{{ $expense->id }}" name="expense_id" id="expense_id" />
                <div class="form-row">
                    <!-- Expense Type Dropdown -->
                    <div class="form-group col-md-4">
                        <label for="expense_type">Expense Type</label>
                        <select name="expense_type" id="expense_type" class="form-control" onchange="toggleOtherExpense(this)" disable>
                            <option value="" {{ old('expense_type',$expense->expense_type) == '' ? 'selected' : '' }}>Expense Type</option>
                            <option value="Food Expense" {{ old('expense_type',$expense->expense_type) == 'Food Expense' ? 'selected' : '' }}>Food Expense</option>
                            <option value="Electricity Bill" {{ old('expense_type',$expense->expense_type) == 'Electricity Bill' ? 'selected' : '' }}>Electricity Bill</option>
                            <option value="Cable Bill" {{ old('expense_type',$expense->expense_type) == 'Cable Bill' ? 'selected' : '' }}>Cable Bill</option>
                            <option value="Internet Bill" {{ old('expense_type',$expense->expense_type) == 'Internet Bill' ? 'selected' : '' }}>Internet Bill</option>
                            <option value="Water Bill" {{ old('expense_type',$expense->expense_type) == 'Water Bill' ? 'selected' : '' }}>Water Bill</option>
                            <option value="Maintenance" {{ old('expense_type',$expense->expense_type) == 'Maintenance' ? 'selected' : '' }}>Maintenance</option>
                            <option value="Employee Salaries" {{ old('expense_type',$expense->expense_type) == 'Employee Salaries' ? 'selected' : '' }}>Employee Salaries</option>
                            <option value="Property Rent" {{ old('expense_type',$expense->expense_type) == 'Property Rent' ? 'selected' : '' }}>Property Rent</option>
                            <option value="Property Tax" {{ old('expense_type',$expense->expense_type) == 'Property Tax' ? 'selected' : '' }}>Property Tax</option>
                            <option value="Others" {{ old('expense_type',$expense->expense_type) == 'Others' ? 'selected' : '' }}>Others</option>
                            <option value="Groceries" {{ old('expense_type',$expense->expense_type) == 'Groceries' ? 'selected' : '' }}>Groceries</option>
                            <option value="Gas" {{ old('expense_type',$expense->expense_type) == 'Gas' ? 'selected' : '' }}>Gas</option>
                            <option value="Housekeeping" {{ old('expense_type',$expense->expense_type) == 'Housekeeping' ? 'selected' : '' }}>Housekeeping</option>
                            <option value="Promotions" {{ old('expense_type',$expense->expense_type) == 'Promotions' ? 'selected' : '' }}>Promotions</option>
                            <option value="Miscellaneous" {{ old('expense_type',$expense->expense_type) == 'Miscellaneous' ? 'selected' : '' }}>Miscellaneous</option>
                            <option value="Purchases" {{ old('expense_type',$expense->expense_type) == 'Purchases' ? 'selected' : '' }}>Purchases</option>
                            <option value="Transport" {{ old('expense_type',$expense->expense_type) == 'Transport' ? 'selected' : '' }}>Transport</option>
                            <option value="Telephone Bill" {{ old('expense_type',$expense->expense_type) == 'Telephone Bill' ? 'selected' : '' }}>Telephone Bill</option>
                            <option value="Water Tanker" {{ old('expense_type',$expense->expense_type) == 'Water Tanker' ? 'selected' : '' }}>Water Tanker</option>
                            <option value="Staff Advance" {{ old('expense_type',$expense->expense_type) == 'Staff Advance' ? 'selected' : '' }}>Staff Advance</option>
                        </select>
                    </div>

                    <!-- Other Expense Type Input -->
                    @if($expense->other_expense_type!='')
                    <div class="form-group col-md-4" id="other_expense_group">
                        <label for="other_expense_type">Other Expense Type</label>
                        <input type="text" class="form-control" id="other_expense_type" name="other_expense_type" value="{{ old('other_expense_type',$expense->other_expense_type) }}" placeholder="Other Expense Type" readonly/>
                    </div>
                    @endif
                    <!-- Expense Recurrence Dropdown -->
                    <div class="form-group col-md-4">
                        <label for="expense_recurrence">Expense Recurrence</label>
                        <select name="expense_recurrence" id="expense_recurrence" class="form-control" disable>
                            <option value="" {{ old('expense_recurrence',$expense->expense_recurrence) == '' ? 'selected' : '' }}>Expense Recurrence</option>
                            <option value="Daily" {{ old('expense_recurrence',$expense->expense_recurrence) == 'Daily' ? 'selected' : '' }}>Daily</option>
                            <option value="Weekly" {{ old('expense_recurrence',$expense->expense_recurrence) == 'Weekly' ? 'selected' : '' }}>Weekly</option>
                            <option value="Monthly" {{ old('expense_recurrence',$expense->expense_recurrence) == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                            <option value="Quarterly" {{ old('expense_recurrence',$expense->expense_recurrence) == 'Quarterly' ? 'selected' : '' }}>Quarterly</option>
                            <option value="Yearly" {{ old('expense_recurrence',$expense->expense_recurrence) == 'Yearly' ? 'selected' : '' }}>Yearly</option>
                            <option value="One-Time" {{ old('expense_recurrence',$expense->expense_recurrence) == 'One-Time' ? 'selected' : '' }}>One-Time</option>
                        </select>
                    </div>

                    <!-- Amount Input -->
                    <div class="form-group col-md-4">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount',$expense->amount) }}" placeholder="Amount" readonly/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="amount">Date</label>
                        <input type="text" class="form-control air-datepicker" id="date" name="date" value="{{ old('date',$expense->date) }}" placeholder="Date" readonly/>
                    </div>
                </div>

                <!-- Comment Textarea -->
                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea class="form-control" name="comment" id="comment" readonly>{!! old('comment',$expense->comments) !!}</textarea>
                </div>
                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#expenseCreation").validate({
            rules: {
                name: "required",
                amount: {
                    required: true,
                    digits: true,
                },
            },
            messages: {
                name: "Please Enter Name",
                amount: {
                    required: "This field is required",
                    digits: "Please enter only digits",
                },
            }
        });
    });
</script>
@endsection
