@extends('layouts.master') @php $page_title = 'Bulding Booking'; $page_name = 'Bulding Booking'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Add Bulding</h6>
            <a href="{{ route('buildings.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('buildings.store') }}" id="buildingCreation">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="area_id">Area ID</label>
                        {!! Form::select('area_id', $areas, old('area_id'), ['class' => 'form-control', 'id' => 'area_id', 'placeholder' => 'Select Area']) !!}
                    </div>
                    <div class="form-group col-md-4">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Building Name" required />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="building_number">Building Number</label>
                        <input type="text" class="form-control" id="building_number" name="building_number" value="{{ old('building_number') }}" placeholder="Building Number" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="number_of_room">Number of Rooms</label>
                        <input type="text" class="form-control" id="number_of_room" name="number_of_room" value="{{ old('number_of_room') }}" placeholder="Number of Rooms" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="building_contact_number">Building Contact Number</label>
                        <input type="text" class="form-control" id="building_contact_number" name="building_contact_number" value="{{ old('building_contact_number') }}" placeholder="Building Contact Number" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="building_type">Building Type</label>
                        <select class="form-control" id="building_type" name="building_type">
                            <option value="">Building Type</option>
                            <option value="own" {{ old('building_type') == 'own' ? 'selected' : '' }}>Own</option>
                            <option value="contract" {{ old('building_type') == 'contract' ? 'selected' : '' }}>Contract</option>
                        </select>
                    </div>
                </div>
                <div class="form-row mb-3">
                    <label for="building_type">Address</label>
                    <textarea name="address" id="address" row=4 class="form-control">{{old('address')}}</textarea>
                </div>
                <!-- Contract Details Card - initially hidden -->
                <div id="contractDetailsCard" style="display: none;">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header py-3 d-flex justify-content-between align-items-center mb-3">
                                <h6 class="m-0 font-weight-bold text-primary">Contract Detail</h6>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="building_owner_name">Building Owner Name</label>
                                    <input type="text" class="form-control" id="building_owner_name" name="building_owner_name" value="{{ old('building_owner_name') }}" placeholder="Building Owner Name" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="building_owner_contact_number">Building Owner Contact Number</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="building_owner_contact_number"
                                        name="building_owner_contact_number"
                                        value="{{ old('building_owner_contact_number') }}"
                                        placeholder="Building Owner Contact Number"
                                    />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="building_advance_amount">Building Advance Amount</label>
                                    <input type="text" class="form-control" id="building_advance_amount" name="building_advance_amount" value="{{ old('building_advance_amount') }}" placeholder="Building Advance Amount" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="contract_fromdate">Contract From Date</label>
                                    <input type="date" class="form-control air-datepicker" autocomplete="off" id="contract_fromdate" name="contract_fromdate" data-date-format="yyyy-mm-dd" value="{{ old('contract_fromdate') }}" readonly/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contract_todate">Contract To Date</label>
                                    <input type="date" class="form-control air-datepicker" autocomplete="off" data-date-format="yyyy-mm-dd" id="contract_todate" name="contract_todate" value="{{ old('contract_todate') }}" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->

<!-- Page level custom scripts -->
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    if ($.fn.datepicker !== undefined) {
        $('.air-datepicker').datepicker({
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'Y-m-d',
                firstDay: 0
            }
        });
    }
</script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        const buildingTypeSelect = document.getElementById("building_type");
        const contractDetailsCard = document.getElementById("contractDetailsCard");

        // Initial toggle based on selected value
        toggleContractDetails();

        // Event listener for select change
        buildingTypeSelect.addEventListener("change", function () {
            toggleContractDetails();
        });

        // Function to toggle visibility
        function toggleContractDetails() {
            if (buildingTypeSelect.value === "contract") {
                contractDetailsCard.style.display = "block";
            } else {
                contractDetailsCard.style.display = "none";
            }
        }
    });
</script>
<script>
    $(document).ready(function () {
        $("#buildingCreation").validate({
            rules: {
                name: "required",
                area_id: "required",
                address: "required",
                building_number: "required",
                number_of_room: {
                    required: true,
                    digits: true,
                },
                building_contact_number: {
                    required: true,
                    digits: true,
                },
                building_owner_name: {
                    required: function (element) {
                        return $("#building_type").val() === "contract";
                    },
                },
                building_owner_contact_number: {
                    required: function (element) {
                        return $("#building_type").val() === "contract";
                    },
                    digits: true,
                },
                building_advance_amount: {
                    required: function (element) {
                        return $("#building_type").val() === "contract";
                    },
                    digits: true,
                },
                contract_fromdate: {
                    required: function (element) {
                        return $("#building_type").val() === "contract";
                    },
                },
                contract_todate: {
                    required: function (element) {
                        return $("#building_type").val() === "contract";
                    },
                },
            },
            messages: {
                name: "Please enter Building Name",
                area_id: "Please select Area",
                address: "Please enter Building Address",
                building_number: "Please enter Building Number",
                number_of_room: {
                    required: "Please enter Number of Rooms",
                    digits: "Please enter only digits",
                },
                building_contact_number: {
                    required: "Please enter Building Contact Number",
                    digits: "Please enter only digits",
                },
                building_owner_name: "Please enter Building Owner Name",
                building_owner_contact_number: {
                    required: "Please enter Building Owner Contact Number",
                    digits: "Please enter only digits",
                },
                building_advance_amount: {
                    required: "Please enter Building Advance Amount",
                    digits: "Please enter only digits",
                },
                contract_fromdate: "Please enter Contract From Date",
                contract_todate: "Please enter Contract To Date",
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "contract_fromdate" || element.attr("name") == "contract_todate") {
                    error.insertAfter("#contractDetailsCard .card-body"); // Place error message after the contract details card
                } else {
                    error.insertAfter(element); // Default placement for other fields
                }
            },
        });
    });
</script>
@endsection
