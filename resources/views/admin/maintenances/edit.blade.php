@extends('layouts.master') @php $page_title = 'Maintenance || Edit'; $page_name = 'Maintenance || Edit'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Edit Maintenance</h6>
            <a href="{{ route('maintenances.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('maintenances.update', $maintenance->id) }}" id="maintenanceCreation" enctype="multipart/form-data">
                @csrf @method('PUT')
                <div class="form-row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputCity">Title</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{old('name',$maintenance->name)}}" placeholder="Title" />
                                </div>
                                <div class="mb-3">
                                    <label for="inputCity">Amount</label>
                                    <input type="text" class="form-control" id="amount" name="amount" value="{{old('amount',$maintenance->amount)}}" placeholder="Amount" />
                                    <div class="mb-3">
                                        <label for="date">Date</label>
                                        <input
                                            type="text"
                                            autocomplete="off"
                                            name="date"
                                            id="date"
                                            placeholder="Date"
                                            class="form-control air-datepicker"
                                            data-date-format="yyyy-mm-dd"
                                            value="{{ old('date',$maintenance->date) }}"
                                            readonly
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="inputCity">Reason</label>
                                    <input type="text" class="form-control" id="reason" name="reason" value="{{old('reason',$maintenance->reason)}}" placeholder="Reason" />
                                </div>
                                <div class="mb-3">
                                    <label for="inputCity">Paid Amount</label>
                                    <input type="text" class="form-control" id="paid_amount" name="paid_amount" value="{{old('paid_amount',$maintenance->paid_amount)}}" placeholder="Paid Amount" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="photo_copy">Bill Photo Copy</label>
                            <input type="file" id="photo_copy" name="photo_copy" class="dropify" data-default-file="{{ $maintenance->photo_copy ? asset('storage/' . $maintenance->photo_copy) : 'path_to_default_image' }}" />
                            <div id="photo_copy_error" class="invalid-feedback d-block">{{ $errors->first('photo') }}</div>
                            <div id="profile_image_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script>
    if ($.fn.datepicker !== undefined) {
        $(".air-datepicker").datepicker({
            language: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                today: "Today",
                clear: "Clear",
                dateFormat: "Y-m-d",
                firstDay: 0,
            },
        });
    }
    $(document).ready(function () {
        $("#maintenanceCreation").validate({
            rules: {
                name: "required",
                reason: "required",
                date: {
                    required: true,
                },
                amount: {
                    required: true,
                    digits: true
                },
                paid_amount: {
                    required: true,
                    digits: true
                },
            },
            messages: {
                name: "Please Enter Name",
                reason: "Please Enter Reason",
                date: {
                    required: "Please Select Type",
                },
                amount: {
                    required: "Please enter the deposit amount",
                    digits: "Please enter a valid number"
                },
                paid_amount: {
                    required: "Please enter the deposit amount",
                    digits: "Please enter a valid number"
                }
            },
        });
    });
    $(".dropify").dropify({
        messages: {
            default: "Drag and drop a file here or click",
            replace: "Drag and drop or click to replace",
            remove: "Remove",
            error: "Oops! Something went wrong. Please try again.",
            imgPlaceholder: "Click to upload image",
        },
        error: {
            fileSize: "The file size is too big (max 1MB).",
            fileExtension: "Only image files (jpg, jpeg, png) are allowed.",
        },
        maxFileSize: "1M",
        allowedFileExtensions: ["jpg", "jpeg", "png"],
        tpl: {
            wrap: '<div class="dropify-wrapper"></div>',
            loader: '<div class="dropify-loader"></div>',
            message: '<div class="dropify-message"><span class="file-icon" /></div>',
            preview: '<div class="dropify-preview"><span class="dropify-render"></span></div>',
            filename: '<div class="dropify-filename"><span class="file-icon" /></div>',
            clearButton: '<button type="button" class="dropify-clear">Remove</button>',
            errorLine: '<div class="dropify-error"></div>',
            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>',
        },
    });
</script>
@endsection
