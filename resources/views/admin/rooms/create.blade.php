@extends('layouts.master') @php $page_title = 'Room Booking'; $page_name = 'Room Booking'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Add Room</h6>
            <a href="{{ route('admin.rooms.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('rooms.store') }}" id="roomCreation">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputCity">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Room Name"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputCity">Door No</label>
                        <input type="text" class="form-control" id="door_no" name="door_no" value="{{old('door_no')}}" placeholder="Door No"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputCity">Max Member</label>
                        <input type="text" class="form-control" id="max_no" name="max_no" value="{{old('max_no')}}" placeholder="Max Member"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputCity">Type</label>
                        <select name="type" class="form-control" id="type" >
                            <option value="">--select Type--</option>
                            <option value="ac" {{ old('type', $record->type ?? '') == "ac" ? 'selected' : '' }}>AC</option>
                            <option value="non-ac" {{ old('type', $record->type ?? '') == "non-ac" ? 'selected' : '' }}>Non AC</option>
                        </select>
                    </div>
                </div>
               
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->

<!-- Page level custom scripts -->
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    var minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    var maxDate = new Date();
    maxDate.setMonth(maxDate.getMonth() + 2); // Move to the month after the next
    maxDate.setDate(0); // Set to the last day of the previous month (which is the end of the next month from today)
    if ($.fn.datepicker !== undefined) {
        $(".air-datepicker").datepicker({
            language: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                today: "Today",
                clear: "Clear",
                dateFormat: "Y-m-d",
                firstDay: 0,
            },
            minDate: minDate, // Set minimum date to today
            maxDate: maxDate, // Set maximum date to the end of next two months
        });
    }
    $(document).ready(function () {
        $("#roomCreation").validate({
            rules: {
                name: "required",
                door_no: {
                    required: true,
                },
                type: {
                    required: true,
                },
                max_no: {
                    required: true,
                    digits: true
                },
            },
            messages: {
                name: "Please Enter Name",
                door_no: {
                    required: "Please Entre Door Number",
                },
                type: {
                    required: "Please Select Type",
                },
                max_no: {
                        required: "This field is required",
                        digits: "Please enter only digits"
                    }
            },
        });
    });
</script>
@endsection
