@extends('layouts.master') @php $page_title = 'Room Booking'; $page_name = 'Room Booking'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Edit Room</h6>
            <a href="{{ route('admin.rooms.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('rooms.update', $room->id) }}" id="roomCreation">
                @csrf @method('PUT')
                <input type="hidden" value="{{ $room->id }}" name="room_id" id="room_id" />
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputCity">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $room->name) }}" placeholder="Room Name" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity">Door No</label>
                        <input type="text" class="form-control" id="door_no" name="door_no" value="{{ old('door_no', $room->door_no) }}" placeholder="Door No" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity">Max Member</label>
                        <input type="text" class="form-control" id="max_no" name="max_no" value="{{ old('max_no', $room->max_no) }}" placeholder="Max Member" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputCity">Type</label>
                        <select name="type" class="form-control" id="type">
                            <option value="">--select Type--</option>
                            <option value="ac" {{ old('type', $room->type ?? '') == "ac" ? 'selected' : '' }}>AC</option>
                            <option value="non-ac" {{ old('type', $room->type ?? '') == "non-ac" ? 'selected' : '' }}>Non AC</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity">Status</label>
                        <select name="status" class="form-control" id="status">
                            <option value="">--select Status--</option>
                            <option value="1" {{ old('status', $room->status ?? '') == "1" ? 'selected' : '' }}>Active</option>
                            <option value="2" {{ old('status', $room->status ?? '') == "2" ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#roomCreation").validate({
            rules: {
                name: "required",
                door_no: {
                    required: true,
                },
                type: {
                    required: true,
                },
                max_no: {
                    required: true,
                    digits: true,
                },
            },
            messages: {
                name: "Please Enter Name",
                door_no: {
                    required: "Please Entre Door Number",
                },
                type: {
                    required: "Please Select Type",
                },
                max_no: {
                    required: "This field is required",
                    digits: "Please enter only digits",
                },
            },
        });
    });
</script>
@endsection
