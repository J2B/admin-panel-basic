@extends('layouts.master') @php $page_title = 'All Room-List'; $page_name = 'All Room-List'; @endphp @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">All Room</h6>
            <a href="{{ route('admin.rooms.create') }}">
                <button type="button" class="btn btn-success" id="cancelButton">Add New</button>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Door No</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Door No</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($rooms as $room)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{$room->name ?? ''}}</td>
                            <td>{{$room->door_no ?? ''}}</td>
                            <td>{!! $room->status == 1 ? '<span class="badge bg-success">Active</span> ' :'<span class="badge bg-danger">InActive</span>' !!}</td>
                            <td>{{$room->created_at ? date('d M-y',strtotime($room->created_at)) : ''}}</td>
                            <td>
                                <a href="{{ route('rooms.edit',$room->id) }}" class="btn btn-warning btn-sm keychainify-checked"><i class="fas fa-pen-square fa-fw"></i></a>
                                {!! Form::open(['method' => 'DELETE','route' => ['rooms.destroy', $room->id],'style'=>'display:inline']) !!}
                                <button type="submit" style="border: 0px;" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm"><i class="fas fa-trash fa-fw"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@endsection
