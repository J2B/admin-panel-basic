@extends('layouts.master') @php $page_title = 'Area Booking'; $page_name = 'Area Booking'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Edit Area</h6>
            <a href="{{ route('areas.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('areas.update', $area->id) }}" id="areaCreation">
                @csrf @method('PUT')
                <input type="hidden" value="{{ $area->id }}" name="area_id" id="area_id" />
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputCity">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $area->name) }}" placeholder="Area Name" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity">Pincode</label>
                        <input type="text" class="form-control" id="pincode" name="pincode" value="{{old('pincode',$area->pincode)}}" placeholder="Pincode"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity">Status</label>
                        <select name="status" class="form-control" id="status">
                            <option value="">--select Status--</option>
                            <option value="1" {{ old('status', $area->status ?? '') == "1" ? 'selected' : '' }}>Active</option>
                            <option value="2" {{ old('status', $area->status ?? '') == "2" ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    @if($area->creted_by!='')
                        <div class="form-group col-md-4">
                            <label for="inputCity">Created By</label>
                            <input type="text" class="form-control"  value="{{$user[$area->creted_by] ?? ''}}" readonly/>
                        </div>
                    @endif
                    
                </div>
                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#areaCreation").validate({
            rules: {
                name: "required",
                amount: {
                    required: true,
                    digits: true,
                },
            },
            messages: {
                name: "Please Enter Name",
                amount: {
                    required: "This field is required",
                    digits: "Please enter only digits",
                },
            }
        });
    });
</script>
@endsection
