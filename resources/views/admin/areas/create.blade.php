@extends('layouts.master') @php $page_title = 'Area Booking'; $page_name = 'Area Booking'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Add Area</h6>
            <a href="{{ route('areas.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('areas.store') }}" id="areaCreation">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputCity">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Area Name"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputCity">Pincode</label>
                        <input type="text" class="form-control" id="pincode" name="pincode" value="{{old('pincode')}}" placeholder="Pincode"/>
                    </div>
                </div>
               
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->

<!-- Page level custom scripts -->
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#areaCreation").validate({
            rules: {
                name: "required",
                pincode: {
                    required: true,
                    digits: true,
                },
            },
            messages: {
                name: "Please Enter Name",
                pincode: {
                    required: "This field is required",
                    digits: "Please enter only digits",
                },
            }
        });
    });
</script>
@endsection
