@extends('layouts.master')

@php 
$page_title = 'Client Booking'; 
$page_name = 'Client Booking'; 
@endphp 

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
@endsection 

@section('content')
<div class="container-fluid">
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif 

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Add Client</h6>
            <a href="{{ route('clients.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('clients.store') }}" id="clientCreation" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Client Name"/>
                                </div>
                                <div class="mb-3">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email"/>
                                </div>
                                <div class="mb-3">
                                    <label for="occupation">Occupation</label>
                                    <input type="text" class="form-control" id="occupation" name="occupation" value="{{ old('occupation') }}" placeholder="Occupation"/>
                                </div>
                                <div class="mb-3">
                                    <label for="id_proof_number">Id Proof Number</label>
                                    <input type="text" class="form-control" id="id_proof_number" name="id_proof_number" value="{{ old('id_proof_number') }}" placeholder="ID Proof Number"/>
                                </div>
                                
                                <div class="mb-3">
                                    <label for="residential_address">Residential Address</label>
                                    <textarea class="form-control" name="residential_address" id="residential_address" rows="4" placeholder="Enter The Residential Address..">{{ old('residential_address') }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label for="room_id">Room</label>
                                    {!! Form::select('room_id', $rooms->pluck('name', 'id'), old('room_id'), ['class' => 'form-control', 'id' => 'room_id', 'placeholder' => 'Select Room']) !!}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="mobile">Mobile</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile"/>
                                </div>
                                <div class="mb-3">
                                    <label for="father_name">Father Name</label>
                                    <input type="text" class="form-control" id="father_name" name="father_name" value="{{ old('father_name') }}" placeholder="Father Name"/>
                                </div>
                                <div class="mb-3">
                                    <label for="id_proof_type">Id Proof Type</label>
                                    <select name="id_proof_type" class="form-control" id="id_proof_type">
                                        <option value="">--select Type--</option>
                                        <option value="Student ID" {{ old('id_proof_type') == 'Student ID' ? 'selected' : '' }}>Student ID</option>
                                        <option value="Passport" {{ old('id_proof_type') == 'Passport' ? 'selected' : '' }}>Passport</option>
                                        <option value="Driving License" {{ old('id_proof_type') == 'Driving License' ? 'selected' : '' }}>Driving License</option>
                                        <option value="Aadhar Card" {{ old('id_proof_type') == 'Aadhar Card' ? 'selected' : '' }}>Aadhar Card</option>
                                        <option value="Ration card" {{ old('id_proof_type') == 'Ration card' ? 'selected' : '' }}>Ration card</option>
                                        <option value="Voter ID" {{ old('id_proof_type') == 'Voter ID' ? 'selected' : '' }}>Voter ID</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="deposit">Deposit</label>
                                    <input type="text" class="form-control" id="deposit" name="deposit" value="{{ old('deposit') }}" placeholder="Deposit"/>
                                </div>
                                <div class="mb-3">
                                    <label for="address">Address</label>
                                    <textarea class="form-control" name="address" id="address" rows="4" placeholder="Enter The Address..">{{ old('address') }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label for="booking_date">Booking Date</label>
                                    <input type="text" autocomplete="off" name="booking_date" id="booking_date" placeholder="booking_date Date" class="form-control air-datepicker" data-date-format="yyyy-mm-dd" value="{{ old('booking_date') }}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="mb-3">
                            <label for="photo">Client Photo</label>
                            <input type="file" id="photo" name="photo" class="dropify" data-default-file="path_to_default_image" />
                            <div id="photo_error" class="invalid-feedback d-block">{{ $errors->first('photo') }}</div>
                            <div id="profile_image_error" class="invalid-feedback"></div>
                        </div>
                        <div class="mb-3">
                            <label for="id_proof_photo">Id Proof Photo</label>
                            <input type="file" id="id_proof_photo" name="id_proof_photo" class="dropify" data-default-file="path_to_default_image" />
                            <div id="id_proof_photo_error" class="invalid-feedback d-block">{{ $errors->first('id_proof_photo') }}</div>
                            <div id="profile_image_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection 

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    if ($.fn.datepicker !== undefined) {
        $('.air-datepicker').datepicker({
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'Y-m-d',
                firstDay: 0
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        $("#clientCreation").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10
                },
                occupation: "required",
                id_proof_number: "required",
                room_id: "required",
                residential_address: "required",
                father_name: "required",
                id_proof_type: "required",
                deposit: {
                    required: true,
                    digits: true
                },
                address: "required",
                booking_date: "required",
                photo: {
                    required: true,
                    extension: "jpg|jpeg|png",
                    filesize: 1048576 // 1MB in bytes
                },
                id_proof_photo: {
                    required: true,
                    extension: "jpg|jpeg|png",
                    filesize: 1048576 // 1MB in bytes
                }
            },
            messages: {
                name: "Please enter the client's name",
                email: {
                    required: "Please enter the client's email",
                    email: "Please enter a valid email address"
                },
                mobile: {
                    required: "Please enter the client's mobile number",
                    digits: "Please enter only digits",
                    minlength: "Mobile number should be 10 digits",
                    maxlength: "Mobile number should be 10 digits"
                },
                occupation: "Please enter the client's occupation",
                id_proof_number: "Please enter the ID proof number",
                room_id: "Please select a room",
                residential_address: "Please enter the residential address",
                father_name: "Please enter the father's name",
                id_proof_type: "Please select the ID proof type",
                deposit: {
                    required: "Please enter the deposit amount",
                    digits: "Please enter a valid number"
                },
                address: "Please enter the address",
                booking_date: "Please Select Booking Date",
                photo: {
                    required: "Please upload a client photo",
                    extension: "Only JPEG and PNG files are allowed",
                    filesize: "File size must be less than 1MB"
                },
                id_proof_photo: {
                    required: "Please upload an ID proof photo",
                    extension: "Only JPEG and PNG files are allowed",
                    filesize: "File size must be less than 1MB"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "photo") {
                    error.appendTo("#photo_error");
                } else if (element.attr("name") == "id_proof_photo") {
                    error.appendTo("#id_proof_photo_error");
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $(".dropify").dropify({
            messages: {
                default: "Drag and drop a file here or click",
                replace: "Drag and drop or click to replace",
                remove: "Remove",
                error: "Oops! Something went wrong. Please try again.",
                imgPlaceholder: "Click to upload image",
            },
            error: {
                fileSize: "The file size is too big (max 1MB).",
                fileExtension: "Only image files (jpg, jpeg, png) are allowed.",
            },
            maxFileSize: "1M",
            allowedFileExtensions: ["jpg", "jpeg", "png"],
            tpl: {
                wrap: '<div class="dropify-wrapper"></div>',
                loader: '<div class="dropify-loader"></div>',
                message: '<div class="dropify-message"><span class="file-icon" /></div>',
                preview: '<div class="dropify-preview"><span class="dropify-render"></span></div>',
                filename: '<div class="dropify-filename"><span class="file-icon" /></div>',
                clearButton: '<button type="button" class="dropify-clear">Remove</button>',
                errorLine: '<div class="dropify-error"></div>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>',
            },
        });

        $.validator.addMethod("extension", function(value, element, param) {
            param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
            return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
        }, $.validator.format("Please enter a valid extension."));

        $.validator.addMethod("filesize", function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param);
        }, $.validator.format("File size must be less than {0} bytes."));
    });
</script>
@endsection
