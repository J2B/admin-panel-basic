@extends('layouts.master')

@php 
    $page_title = 'Client Edit'; 
    $page_name = 'Client Edit'; 
@endphp 

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet" />
@endsection 

@section('content')
<div class="container-fluid">
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif 

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">View Client</h6>
            <a href="{{ route('clients.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('checkout-client') }}" id="clientCreation" enctype="multipart/form-data">
                @csrf 
                @method('POST')
                <div class="form-row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- Client Details -->
                                <div class="mb-3">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $client->name) }}" placeholder="Client Name" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $client->email) }}" placeholder="Email" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="occupation">Occupation</label>
                                    <input type="text" class="form-control" id="occupation" name="occupation" value="{{ old('occupation', $client->occupation) }}" placeholder="Occupation" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="id_proof_number">Id Proof Number</label>
                                    <input type="text" class="form-control" id="id_proof_number" name="id_proof_number" value="{{ old('id_proof_number', $client->id_proof_number) }}" placeholder="ID Proof Number" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="residential_address">Residential Address</label>
                                    <textarea class="form-control" name="residential_address" id="residential_address" rows="4" placeholder="Enter The Residential Address.." readonly>{{ old('residential_address', $client->residential_address) }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label for="room_id">Room</label>
                                    {!! Form::select('room_id', $rooms, old('room_id', $client->room_id), ['class' => 'form-control', 'id' => 'room_id', 'placeholder' => 'Select Room', 'disabled']) !!}
                                </div>
                                <div class="mb-3">
                                    <label for="checkout_date">Checkout Date</label>
                                    <input type="text" autocomplete="off" name="checkout_date" id="checkout_date" placeholder="Checkout Date" class="form-control air-datepicker" data-date-format="yyyy-mm-dd" value="{{ old('checkout_date') }}" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- More Client Details -->
                                <div class="mb-3">
                                    <label for="mobile">Mobile</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile', $client->mobile) }}" placeholder="Mobile" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="father_name">Father Name</label>
                                    <input type="text" class="form-control" id="father_name" name="father_name" value="{{ old('father_name', $client->father_name) }}" placeholder="Father Name" disabled/>
                                </div>
                                <div class="mb-3">
                                    <label for="id_proof_type">Id Proof Type</label>
                                    <select name="id_proof_type" class="form-control" id="id_proof_type" disabled>
                                        <option value="">--select Type--</option>
                                        <option value="Student ID" {{ old('id_proof_type', $client->id_proof_type) == 'Student ID' ? 'selected' : '' }}>Student ID</option>
                                        <option value="Passport" {{ old('id_proof_type', $client->id_proof_type) == 'Passport' ? 'selected' : '' }}>Passport</option>
                                        <option value="Driving License" {{ old('id_proof_type', $client->id_proof_type) == 'Driving License' ? 'selected' : '' }}>Driving License</option>
                                        <option value="Aadhar Card" {{ old('id_proof_type', $client->id_proof_type) == 'Aadhar Card' ? 'selected' : '' }}>Aadhar Card</option>
                                        <option value="Ration card" {{ old('id_proof_type', $client->id_proof_type) == 'Ration card' ? 'selected' : '' }}>Ration card</option>
                                        <option value="Voter ID" {{ old('id_proof_type', $client->id_proof_type) == 'Voter ID' ? 'selected' : '' }}>Voter ID</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="client_id">Client ID</label>
                                    <input type="text" class="form-control" id="client_id" name="client_id" value="{{ old('client_id', $client->client_id) }}" placeholder="Client Id" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="address">Address</label>
                                    <textarea class="form-control" name="address" id="address" rows="4" placeholder="Enter The Address.." readonly>{{ old('address', $client->address) }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label for="created_by">Created By</label>
                                    <input type="text" class="form-control" value="{{ old('created_by', $user[$client->created_by] ?? '') }}" placeholder="Client Id" readonly/>
                                </div>
                                <div class="mb-3">
                                    <label for="return_deposit">Return Amount</label>
                                    <input type="text" class="form-control" id="return_deposit" name="return_deposit" value="{{ old('return_deposit') }}" placeholder="Return Deposit" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <!-- Client Photo and ID Proof -->
                        <div class="mb-3">
                            <label for="photo">Client Photo</label>
                            <input type="file" id="photo" name="photo" class="dropify" data-default-file="{{ $client->photo ? asset('storage/' . $client->photo) : 'path_to_default_image' }}" disabled />
                            <div id="photo_error" class="invalid-feedback d-block">{{ $errors->first('photo') }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="id_proof_photo">Id Proof Photo</label>
                            <input type="file" id="id_proof_photo" name="id_proof_photo" class="dropify" data-default-file="{{ $client->id_proof_photo ? asset('storage/' . $client->id_proof_photo) : 'path_to_default_image' }}" disabled />
                            <div id="id_proof_photo_error" class="invalid-feedback d-block">{{ $errors->first('id_proof_photo') }}</div>
                        </div>
                        <div class="mb-3">
                            <label for="deposit">Deposit</label>
                            <input type="text" class="form-control" id="deposit" name="deposit" value="{{ old('deposit', $client->deposit) }}" placeholder="Deposit" readonly/>
                        </div>
                        <div class="mb-3">
                            <label for="reason">Reason</label>
                            <input type="hidden" name="client_id" value="{{ $client->id }}">
                            <input type="text" class="form-control" id="reason" name="reason" value="{{ old('reason') }}" placeholder="Reason" />
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Check Out</button>
            </form>
        </div>
    </div>
</div>
@endsection 

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script>
        if ($.fn.datepicker !== undefined) {
            $('.air-datepicker').datepicker({
                language: {
                    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                    daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    today: 'Today',
                    clear: 'Clear',
                    dateFormat: 'yyyy-mm-dd',
                    firstDay: 0
                }
            });
        }
    </script>
    <script>
        $(document).ready(function () {
            $("#clientCreation").validate({
                rules: {
                    return_deposit: {
                        required: true,
                        digits: true
                    },
                    reason: "required",
                    checkout_date: "required",
                },
                messages: {
                    return_deposit: {
                        required: "Please enter the deposit amount",
                        digits: "Please enter a valid number"
                    },
                    reason: "Please enter the Reason",
                    checkout_date: "Please Select Check out Date",
                },
            });

            $(".dropify").dropify({
                messages: {
                    default: "Drag and drop a file here or click",
                    replace: "Drag and drop or click to replace",
                    remove: "Remove",
                    error: "Oops! Something went wrong. Please try again.",
                    imgPlaceholder: "Click to upload image",
                },
                error: {
                    fileSize: "The file size is too big (max 1MB).",
                    fileExtension: "Only image files (jpg, jpeg, png) are allowed.",
                },
                maxFileSize: "1M",
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                tpl: {
                    wrap: '<div class="dropify-wrapper"></div>',
                    loader: '<div class="dropify-loader"></div>',
                    message: '<div class="dropify-message"><span class="file-icon" /></div>',
                    preview: '<div class="dropify-preview"><span class="dropify-render"></span></div>',
                    filename: '<div class="dropify-filename"><span class="file-icon" /></div>',
                    clearButton: '<button type="button" class="dropify-clear">Remove</button>',
                    errorLine: '<div class="dropify-error"></div>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>',
                },
            });

            $.validator.addMethod("extension", function(value, element, param) {
                param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            }, $.validator.format("Please enter a valid extension."));

            $.validator.addMethod("filesize", function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param);
            }, $.validator.format("File size must be less than {0} bytes."));
        });
    </script>
@endsection
