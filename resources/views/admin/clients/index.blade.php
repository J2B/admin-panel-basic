@extends('layouts.master') @php $page_title = 'All Client-List'; $page_name = 'All Client-List'; @endphp @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">All Client</h6>
            <a href="{{ route('clients.create') }}">
                <button type="button" class="btn btn-success" id="cancelButton">Add New</button>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>status</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>status</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($clients as $client)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{$client->name ?? ''}}</td>
                            <td>{{$client->email ?? ''}}</td>
                            <td>{{$client->mobile ?? ''}}</td>
                            <td>
                                {!! $client->status == 1 ? '<span class="badge bg-success">Active</span>' : '' !!}
                                {!! $client->status == 2 ? '<span class="badge bg-danger">Inactive</span>' : '' !!}
                                {!! $client->status == 3 ? '<span class="badge bg-info">Check In</span>' : '' !!}
                                {!! $client->status == 4 ? '<span class="badge bg-danger">Check Out</span>' : '' !!}
                            </td>
                            <td>{{ $client->created_at ? date('d M Y', strtotime($client->created_at)) : '' }}</td>

                            <td>
                                <a href="{{ route('clients.edit',$client->id) }}" class="btn btn-warning btn-sm keychainify-checked"><i class="fas fa-pen-square fa-fw"></i></a>
                                @if($client->status != 4)
                                <a href="{{ route('clients.show',$client->id) }}" class="btn btn-danger btn-sm">check out</a>
                                @endif
                                <!-- {!! Form::open(['method' => 'DELETE','route' => ['clients.destroy', $client->id],'style'=>'display:inline']) !!}
                                <button type="submit" style="border: 0px;" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm"><i class="fas fa-trash fa-fw"></i></button>
                                {!! Form::close() !!} -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@endsection
