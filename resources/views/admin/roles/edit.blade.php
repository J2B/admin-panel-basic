@extends('layouts.master') @php $page_title = 'Role'; $page_name = 'Role'; @endphp @section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection @section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <p class="mb-4">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.
        </p> -->

    <!-- DataTales Example -->
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Role</h6>
        </div>
        <div class="card-body">
            {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!} @csrf
            <div class="form-row">
                <div class="form-group col-md-7">
                    <label for="inputCity">Name</label>
                    {!! Form::text('name', $role->name, array('placeholder' => 'Name','class' => 'form-control','required')) !!}
                </div>
                <div class="form-group col-md-3 mt-4 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            <div class="mb-3">
                <label for="product-reference" class="form-label">Permission <span class="text-danger">*</span></label>
                <ul class="permission-list" style="list-style-type: none; grid-template-columns: 1fr 1fr 1fr 1fr; display: grid;">
                    @php $i=1; @endphp @foreach($permission as $value)
                    <li>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'custom-control-input','id'=>'customControlAutosizing'.$i)) }}
                            <label class="custom-control-label" for="customControlAutosizing{{ $i }}">{{ $value->name }}</label>
                            @php $i++; @endphp
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection @section('js')
<!-- Page level plugins -->

<!-- Page level custom scripts -->
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    var minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    var maxDate = new Date();
    maxDate.setMonth(maxDate.getMonth() + 2); // Move to the month after the next
    maxDate.setDate(0); // Set to the last day of the previous month (which is the end of the next month from today)
    if ($.fn.datepicker !== undefined) {
        $(".air-datepicker").datepicker({
            language: {
                days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                today: "Today",
                clear: "Clear",
                dateFormat: "Y-m-d",
                firstDay: 0,
            },
            minDate: minDate, // Set minimum date to today
            maxDate: maxDate, // Set maximum date to the end of next two months
        });
    }
    $(document).ready(function () {
        $("#roleCreation").validate({
            rules: {
                name: "required",
            },
            messages: {
                name: "Please Enter Role Name",
            },
        });
    });
</script>
@endsection
