<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Billing Detail</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
    <style>
        .card-header {
            background-color: #4e73df;
            color: white;
        }
        .section-header {
            background-color: lavender;
            color: brown;
            padding: 10px;
        }
        .total-amount {
            font-size: 1.5em;
            font-weight: bold;
        }
        .row{
            max-width: 830px !important;
        }
        .table td, .table th {
            padding: 0.50rem !important;
        }
    </style>
</head>
<body>
    <!-- <div class="container"> -->
        <div class="card">
            <div class="card-header">
                <h2>{{ $client_detail->month }} Month Billing Detail</h2>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="card p-2" style="width:650px">
                            <h3 class="section-header">Client Information</h3>
                            <div class="row">
                                <table  style="width:100%;" align="center">
                                    <tr>
                                        <td style="width:50%;padding-left: 19px;padding-bottom: 11px;"><strong>Name:</strong> {{ $client->name }}</td>
                                        <td style=" width:50%; padding-left: 19px;padding-bottom: 11px;"><strong>Email:</strong> {{ $client->email }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width:50%;padding-left: 19px;padding-bottom: 11px;"><strong>Phone:</strong> {{ $client->mobile }}</td>
                                        <td style=" width:50%; padding-left: 19px;padding-bottom: 11px;"><strong>Deposit:</strong> {{ $client->deposit }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card p-2 mt-2"  style="width:650px">
                            <h3 class="section-header">Room Information</h3>
                            <div class="row">
                                <table  style="width:100%;" align="center">
                                    <tr>
                                        <td style="width:50%;padding-left: 19px;padding-bottom: 11px;"><strong>Room:</strong> {{ $room->name }}</td>
                                        <td style=" width:50%; padding-left: 19px;padding-bottom: 11px;"><strong>Room No:</strong> {{ $room->door_no }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card p-2"  style="width:650px">
                            <h3 class="section-header">Billing Details</h3>
                            <table class="table table-bordered"style="width:100%; font-size:12px;" align="center">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Rent</td>
                                        <td>{{ $client_detail->rent ?? '0' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Food</td>
                                        <td>{{ $client_detail->food ?? '0' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Paid Amount</td>
                                        <td>{{ $client_detail->paid_amount ?? '0' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Amount</td>
                                        <td>{{ $client_detail->total_amount ?? '0' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Balance Amount</td>
                                        <td>{{ $client_detail->balance_due ?? '0' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Billing Date</td>
                                        <td>{{ $client_detail->billing_date }}</td>
                                    </tr>
                                    <tr>
                                        <td>Electricity</td>
                                        <td>{{ $client_detail->eb_amount ?? '0' }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right;"><strong>Total Amount : {{ $client_detail->total_amount ?? '0' }}</strong></td>
                                    </tr>
                                    <!-- Add more billing items as needed -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- </div> -->
