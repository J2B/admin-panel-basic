@extends('layouts.master')

@php
    $page_title = 'Client Billing Detail';
    $page_name = 'Client Billing Detail';
@endphp

@section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <!-- <h6 class="m-0 font-weight-bold text-primary">Edit Room</h6> -->
            <a href="{{ route('client-details.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
            <a href="{{ route('client-details-create', $client->id) }}">
                <button type="button" class="btn btn-success">New Bill</button>
            </a>
        </div>

        <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="room_id">Room Name</label>
                        <input type="text" class="form-control" value="{{$room[$client->room_id] ?? '' }}" placeholder="Room Name" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="name">Client Name</label>
                        <input type="text" class="form-control" value="{{ $client->name ?? '' }}" placeholder="Client Name" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{ $client->email ?? '' }}" placeholder="Email" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="mobile">Mobile</label>
                        <input type="text" class="form-control" value="{{ $client->mobile ?? '' }}" placeholder="Mobile" readonly />
                    </div>
                </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Month</th>
                            <th>Paid Amount</th>
                            <th>Balace Amount</th>
                            <th>Billing Date</th>
                            <th>Billing Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.no</th>
                            <th>Month</th>
                            <th>Paid Amount</th>
                            <th>Balace Amount</th>
                            <th>Billing Date</th>
                            <th>Billing Type</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach($client_details as $client)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $client->month ?? '' }}</td>
                            <td>{{ $client->paid_amount ?? ''}}</td>
                            <td>{{ $client->old_amount ?? '0' }}</td>
                            <td>{{ $client->billing_date ?? '' }}</td>
                            <td>{{ $client->billing_type ?? '' }}</td>
                            <td>
                                <a href="{{ route('client-details-view', $client->id) }}" class="btn btn-warning btn-sm">
                                    <i class="fas fa-pen-square fa-fw"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @php $i++; @endphp
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/demo/datatables-demo.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });
</script>
@endsection
