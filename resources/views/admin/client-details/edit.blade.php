@extends('layouts.master')

@php
    $page_title = 'Client Detail';
    $page_name = 'Client Detail';
@endphp

@section('css')
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <!-- <h6 class="m-0 font-weight-bold text-primary">Edit Room</h6> -->
            <a href="{{ route('client-details.index') }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
        </div>

        <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $room->name) }}" placeholder="Room Name" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="door_no">Door No</label>
                        <input type="text" class="form-control" id="door_no" name="door_no" value="{{ old('door_no', $room->door_no) }}" placeholder="Door No" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="max_no">Max Member</label>
                        <input type="text" class="form-control" id="max_no" name="max_no" value="{{ old('max_no', $room->max_no) }}" placeholder="Max Member" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="type">Type</label>
                        <input type="text" class="form-control" id="type" name="type" value="{{ old('type', $room->type) }}" placeholder="type" readonly />
                    </div>
                </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th>S.no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach($clients as $client)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->email }}</td>
                            <td>{{ $client->mobile }}</td>
                            <td>
                                <a href="{{ route('client-details.show', $client->id) }}" class="btn btn-warning btn-sm">
                                    <i class="fas fa-pen-square fa-fw"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @php $i++; @endphp
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/demo/datatables-demo.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });
</script>
@endsection
