@extends('layouts.master')

@php
    $page_title = $client_detail->month . ' Billing Detail';
    $page_name = $client_detail->month . ' Billing Detail';
    // Generate list of months
    $months = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
@endphp

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
@endsection

@section('content')
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex justify-content-between align-items-center">
            <a href="{{ url()->previous() }}">
                <button type="button" class="btn btn-danger" id="cancelButton">Back</button>
            </a>
            <a href="{{ route('client-details-download', $client_detail->id) }}">
                <button type="button" class="btn btn-success" id="cancelButton">Download Bill</button>
            </a>
        </div>

        <div class="card-body">
            <!-- <form method="POST" action="{{ route('client-details.store') }}" id="clientCreation" enctype="multipart/form-data">
                @csrf -->
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="room_id">Room Name</label>
                        <input type="hidden" name="client_id" value="{{ $client->id }}">
                        <input type="hidden" name="room_id" value="{{ $client->room_id }}">
                        <input type="text" class="form-control" value="{{ $room[$client_detail->room_id] ?? '' }}" placeholder="Room Name" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="name">Client Name</label>
                        <input type="text" class="form-control" value="{{ $client->name ?? '' }}" placeholder="Client Name" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" value="{{ $client->email ?? '' }}" placeholder="Email" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="mobile">Mobile</label>
                        <input type="text" class="form-control" value="{{ $client->mobile ?? '' }}" placeholder="Mobile" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="deposit">Deposit Amount</label>
                        <input type="text" name="advance" id="advance" class="form-control" value="{{ $client->deposit ?? '' }}" placeholder="Deposit" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="old_due">Balace Amount</label>
                        <input type="text" name="old_due" id="old_due" class="form-control" value="{{ $client_detail->balance_due ?? '0' }}" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="rent">Rent</label>
                        <input type="text" class="form-control" name="rent" id="rent" value="{{ old('rent', $client_detail->rent) }}"placeholder="Rent" readonly/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="food">Food</label>
                        <input type="text" class="form-control" name="food" id="food" value="{{ old('food', $client_detail->food) }}" placeholder="Food" readonly/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="eb_id">Electricity</label>
                        {!! Form::select('eb_id', $electricity, old('eb_id', $client_detail->eb_id), ['class' => 'form-control', 'id' => 'eb_id', 'readonly' => 'readonly']) !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label for="eb_unit">EB Unit</label>
                        <input type="text" class="form-control" name="eb_unit" id="eb_unit" value="{{ old('eb_unit', $client_detail->eb_unit) }}" placeholder="EB Unit" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="eb_amount">EB Amount</label>
                        <input type="text" class="form-control" name="eb_amount" id="eb_amount" value="{{ old('eb_amount', $client_detail->eb_amount) }}" placeholder="EB Amount" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="total_amount">Total Amount</label>
                        <input type="text" class="form-control" name="total_amount" id="total_amount" value="{{ old('total_amount', $client_detail->total_amount) }}" placeholder="Total Amount" readonly />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="month">Month</label>
                        <select name="month" class="form-control" id="month" readonly>
                            <option value="">--select Month--</option>
                            @foreach($months as $month)
                                <option value="{{ $month }}" {{ old('month', $client_detail->month) == $month ? 'selected' : '' }}>{{ $month }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="billing_date">Billing Date</label>
                        <input type="text" autocomplete="off" name="billing_date" id="billing_date" placeholder="Billing Date" class="form-control air-datepicker" data-date-format="yyyy-mm-dd" value="{{ old('billing_date', $client_detail->billing_date) }}" readonly>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="billing_type">Billing Type</label>
                        <select name="billing_type" class="form-control" id="billing_type" readonly>
                            <option value="">--select Type--</option>
                            <option value="Cash" {{ old('billing_type', $client_detail->billing_type) == 'Cash' ? 'selected' : '' }}>Cash</option>
                            <option value="Online Payment" {{ old('billing_type', $client_detail->billing_type) == 'Online Payment' ? 'selected' : '' }}>Online Payment</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="paid_amount">Paid Amount</label>
                        <input type="text" class="form-control" name="paid_amount" id="paid_amount" value="{{ old('paid_amount', $client_detail->paid_amount) }}" placeholder="Paid Amount" />
                    </div>
                    <div class="form-group col-md-3" id="photo_uploader" style="display: none;">
                        <label for="billing_photo_copy">Billing Photo Copy</label>
                        <input type="file" id="billing_photo_copy" name="billing_photo_copy" class="dropify" data-default-file="" />
                        <div id="photo_error" class="invalid-feedback d-block">{{ $errors->first('billing_photo_copy') }}</div>
                        <div id="profile_image_error" class="invalid-feedback"></div>
                    </div>
                </div>
                <!-- <button type="submit" class="btn btn-primary">Submit</button>
            </form> -->
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script>
    if ($.fn.datepicker !== undefined) {
        $('.air-datepicker').datepicker({
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'Y-m-d',
                firstDay: 0
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        $('.dropify').dropify();

        $("#clientCreation").validate({
            rules: {
                advance: "required",
                rent: {
                    required: true,
                    digits: true,
                },
                eb_id: "required",
                eb_unit: {
                    required: true,
                    digits: true,
                },
                room_id: "required",
                month: "required",
                billing_date: "required",
                billing_type: "required",
                paid_amount: {
                    required: true,
                    digits: true
                },
                billing_photo_copy: {
                    required: function(element) {
                        return $('#billing_type').val() === 'Online Payment';
                    },
                    extension: "jpg|jpeg|png",
                    filesize: 1048576 // 1MB in bytes
                }
            },
            messages: {
                advance: "Please enter the Advance",
                rent: {
                    required: "Please enter the client's Rent",
                    digits: "Please enter only digits",
                },
                eb_id: "Please enter the client's EB Name",
                eb_unit: {
                    required: "Please enter the client's EB Unit",
                    digits: "Please enter only digits",
                },
                room_id: "Please select a room",
                month: "Please Select Month",
                billing_date: "Please Select Billing Date",
                billing_type: "Please Select Billing Type",
                paid_amount: {
                    required: "Please enter the Paid amount",
                    digits: "Please enter a valid number"
                },
                billing_photo_copy: {
                    required: "Please upload a Bill Photo",
                    extension: "Only JPEG and PNG files are allowed",
                    filesize: "File size must be less than 1MB"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "billing_photo_copy") {
                    error.appendTo("#photo_error");
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("is-invalid").removeClass("is-valid");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("is-invalid").addClass("is-valid");
            }
        });

        $('#billing_type').on('change', function() {
            var billingType = $(this).val();
            if (billingType == 'Online Payment') {
                $('#photo_uploader').show();
            } else {
                $('#photo_uploader').hide();
            }
        });

        // Trigger change event on page load to handle pre-selected value
        $('#billing_type').trigger('change');

        function calculateTotalAmount() {
            var ebAmount = parseFloat($('#eb_amount').val()) || 0;
            var rentAmount = parseFloat($('#rent').val()) || 0;
            var oldDueAmount = parseFloat($('#old_due').val()) || 0;
            var foodAmount = parseFloat($('#food').val()) || 0;

            var totalAmount = ebAmount + rentAmount + foodAmount + oldDueAmount;

            $('#total_amount').val(totalAmount);
        }

        $('#rent').on('change', calculateTotalAmount);
        $('#food').on('input', calculateTotalAmount);
        $('#old_due').on('input', calculateTotalAmount);

        function calculateEbAmount() {
            var ebUnit = $('#eb_unit').val();
            var ebId = $('#eb_id').val();

            if (ebId !== '') {
                $.ajax({
                    url: '{{ route("calculate.eb.amount") }}',
                    type: 'GET',
                    data: { eb_unit: ebUnit, eb_id: ebId },
                    success: function(response) {
                        if (response.eb_amount == 1) {
                            alert("Please select valid Electricity");
                        } else {
                            $('#eb_amount').val(response.eb_amount);
                            calculateTotalAmount();
                        }
                    }
                });
            } else {
                calculateTotalAmount();
                alert("Please select Electricity");
            }
        }

        $('#eb_id').on('change', calculateEbAmount);
        $('#eb_unit').on('input', calculateEbAmount);
    });
</script>
@endsection
