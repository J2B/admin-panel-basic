<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title>{{ isset($page_title) && $page_title ? $page_title : "Admin" }}</title>
        @include('includes.style') @yield('css')
    </head>
    <body class="bg-gradient-primary">
        @yield('content')     @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error) {{ $error }} @endforeach
            </ul>
        </div>
        @endif @include('includes.script') @yield('js')
    </body>
</html>
