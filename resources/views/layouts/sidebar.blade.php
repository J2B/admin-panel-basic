<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">PG HOSTING</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0" />

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    @canAny(['role-list','role-create','role-edit','role-delete','permission-list','permission-create','permission-edit','permission-delete','user-list','user-create','user-edit','user-delete'])
        <!-- Divider -->
        <hr class="sidebar-divider" />
        <!-- Heading -->
        <div class="sidebar-heading">
            Interface
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
    
        <li class="nav-item {{ areActiveRoutes(['admin.roles.index', 'admin.permissions.index', 'admin.users.index'], 'active') }}">
            <a class="nav-link {{ areActiveRoutes(['admin.roles.index', 'admin.permissions.index', 'admin.users.index'], 'active') ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="{{ areActiveRoutes(['admin.roles.index', 'admin.permissions.index', 'admin.users.index'], 'active') ? 'true' : 'false' }}" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Admin Setting</span>
            </a>
            <div id="collapseTwo" class="collapse {{ areActiveRoutes(['admin.roles.index', 'admin.roles.create', 'admin.permissions.index', 'admin.permissions.create', 'admin.users.index', 'admin.users.create'], 'show') }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                @canAny(['role-list','role-create','role-edit','role-delete'])
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header"><b>Role:</b></h6>
                        <a class="collapse-item {{ isActiveRoute('admin.roles.index', 'active') }}" href="{{ route('admin.roles.index') }}">Role</a>
                        <a class="collapse-item {{ isActiveRoute('admin.roles.create', 'active') }}" href="{{ route('admin.roles.create') }}">Add New Role</a>
                    </div>
                @endcan
                @canAny(['permission-list','permission-create','permission-edit','permission-delete'])
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header"><b>Permission:</b></h6>
                        <a class="collapse-item {{ isActiveRoute('admin.permissions.index', 'active') }}" href="{{ route('admin.permissions.index') }}">Permission</a>
                        <a class="collapse-item {{ isActiveRoute('admin.permissions.create', 'active') }}" href="{{ route('admin.permissions.create') }}">Add New Permission</a>
                    </div>
                @endcan
                @canAny(['user-list','user-create','user-edit','user-delete'])
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header"><b>User:</b></h6>
                        <a class="collapse-item {{ isActiveRoute('admin.users.index', 'active') }}" href="{{ route('admin.users.index') }}">User</a>
                        <!-- <a class="collapse-item {{ isActiveRoute('admin.users.create', 'active') }}" href="{{ route('admin.users.create') }}">Add New User</a> -->
                    </div>
                @endcan
            </div>
        </li>
    @endcan
    <!-- Nav Item - Utilities Collapse Menu -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Utilities:</h6>
                <a class="collapse-item" href="utilities-color.html">Colors</a>
                <a class="collapse-item" href="utilities-border.html">Borders</a>
                <a class="collapse-item" href="utilities-animation.html">Animations</a>
                <a class="collapse-item" href="utilities-other.html">Other</a>
            </div>
        </div>
    </li> -->
        <!-- Divider -->
        @canAny(['room-list','room-create','room-edit','room-delete','eb-list','eb-create','eb-edit','eb-delete'])
            <hr class="sidebar-divider" />

            <!-- Heading -->
            <div class="sidebar-heading">
                CMS
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ areActiveRoutes(['admin.rooms.index','admin.rooms.create','electricities.index','electricities.create','electricities.edit'], 'active') }}">
                <a class="nav-link {{ areActiveRoutes(['admin.rooms.index','admin.rooms.create','electricities.index','electricities.create','electricities.edit'], 'active') ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="{{ areActiveRoutes(['admin.rooms.index','admin.rooms.create','electricities.index','electricities.create','electricities.edit'], 'active') ? 'true' : 'false' }}" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>CMS Details</span>
                </a>
                

                <div id="collapsePages" class="collapse {{ areActiveRoutes(['admin.rooms.index','admin.rooms.create','electricities.index','electricities.create','electricities.edit'], 'show ') }}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    @canAny(['room-list','room-create','room-edit','room-delete'])
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header"><b>Room:</b></h6>
                            @can('room-list')
                                <a class="collapse-item {{ isActiveRoute('admin.rooms.index', 'active') }}" href="{{route('admin.rooms.index')}}">All Room</a>
                            @endcan
                            @can('room-create')
                                <a class="collapse-item {{ isActiveRoute('admin.rooms.create', 'active') }}" href="{{route('admin.rooms.create')}}"> Add Room</a>
                            @endcan
                            <!-- <div class="collapse-divider"></div>
                            <h6 class="collapse-header">Other Pages:</h6>
                            <a class="collapse-item" href="404.html">404 Page</a>
                            <a class="collapse-item" href="blank.html">Blank Page</a> -->
                        </div>
                    @endcan
                    @canAny(['eb-list','eb-create','eb-edit','eb-delete'])
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header"><b>EB:</b></h6>
                            @can('eb-list')
                                <a class="collapse-item {{ areActiveRoutes(['electricities.index','electricities.edit'], 'active') }}" href="{{route('electricities.index')}}">All EB</a>
                            @endcan
                            @can('eb-create')
                                <a class="collapse-item {{ isActiveRoute('electricities.create', 'active') }}" href="{{route('electricities.create')}}"> Add EB</a>
                            @endcan
                        </div>
                    @endcan
                </div>
            </li>
        @endcan

        @canAny(['client-list','client-create','client-edit','client-delete','client_details-list','client_details-create','client_details-edit','client_details-delete'])
            <hr class="sidebar-divider" />

            <!-- Heading -->
            <div class="sidebar-heading">
                CLIENT
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ areActiveRoutes(['clients.index','clients.create','clients.edit','client-details.index','client-details.edit'], 'active') }}">
                <a class="nav-link {{ areActiveRoutes(['clients.index','clients.create','clients.edit','client-details.index','client-details.edit'], 'active') ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#clientPages" aria-expanded="{{ areActiveRoutes(['clients.index','clients.create','clients.edit','client-details.index','client-details.edit'], 'active') ? 'true' : 'false' }}" aria-controls="clientPages">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Client Details</span>
                </a>
                

                <div id="clientPages" class="collapse {{ areActiveRoutes(['clients.index','clients.create','clients.edit','client-details.index','client-details.edit'], 'show ') }}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    @canAny(['client-list','client-create','client-edit','client-delete'])
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header"><b>Client:</b></h6>
                            @can('client-list')
                                <a class="collapse-item {{ areActiveRoutes(['clients.index','clients.edit'], 'active') }}" href="{{route('clients.index')}}">All Client</a>
                            @endcan
                            @can('client-create')
                                <a class="collapse-item {{ isActiveRoute('clients.create', 'active') }}" href="{{route('clients.create')}}"> Add Client</a>
                            @endcan
                        </div>
                    @endcan
                    @canAny(['client_details-list','client_details-create','client_details-edit','client_details-delete'])
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header"><b>Client Detail:</b></h6>
                            @can('client_details-list')
                                <a class="collapse-item {{ areActiveRoutes(['client-details.index','client-details.edit'], 'active') }}" href="{{route('client-details.index')}}">All Client Detail</a>
                            @endcan
                        </div>
                    @endcan
                </div>
            </li>
        @endcan 
        @canAny(['maintenance-list','maintenance-create','maintenance-edit','maintenance-delete'])
            <hr class="sidebar-divider" />

            <!-- Heading -->
            <div class="sidebar-heading">
                Maintenance
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ areActiveRoutes(['maintenances.index','maintenances.create','maintenances.edit'], 'active') }}">
                <a class="nav-link {{ areActiveRoutes(['maintenances.index','maintenances.create','maintenances.edit'], 'active') ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#maintenancePages" aria-expanded="{{ areActiveRoutes(['maintenances.index','maintenances.create','maintenances.edit'], 'active') ? 'true' : 'false' }}" aria-controls="maintenancePages">
                    <i class="fas fa-fw fa-cogs"></i>
                    <span>Maintenance Details</span>
                </a>
                

                <div id="maintenancePages" class="collapse {{ areActiveRoutes(['maintenances.index','maintenances.create','maintenances.edit'], 'show ') }}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    @canAny(['maintenance-list','maintenance-create','maintenance-edit','maintenance-delete'])
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header"><b>Maintenance:</b></h6>
                            @can('maintenance-list')
                                <a class="collapse-item {{ areActiveRoutes(['maintenances.index','maintenances.edit'], 'active') }}" href="{{route('maintenances.index')}}">All Maintenance</a>
                            @endcan
                            @can('maintenance-create')
                                <a class="collapse-item {{ isActiveRoute('maintenances.create', 'active') }}" href="{{route('maintenances.create')}}"> Add Maintenance</a>
                            @endcan
                        </div>
                    @endcan
                </div>
            </li>
        @endcan       <!-- Nav Item - Charts -->
    <!-- <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span>
        </a>
    </li> -->

    <!-- Nav Item - Tables -->
    <!-- <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span>
        </a>
    </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block" />

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    <!-- Sidebar Message -->
    <!-- <div class="sidebar-card d-none d-lg-flex">
        <img class="sidebar-card-illustration mb-2" src="{{asset('assets/img/undraw_rocket.svg')}}" alt="..." />
        <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
        <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
    </div> -->
</ul>
<!-- End of Sidebar -->
